@skip
Feature: RECV Component - Fail
Scenario Outline: Invalid configuration
    Given K8s manifest is <manifest>.
    When K8s manifest is deployed.
    Then K8s pod logs contains the message ERROR.

    Examples:
    | manifest |
    | k8srunner-bdd-fail |

# Scenario Outline: Connectivity failure
#     Given K8s manifest is <manifest>.
#     When K8s manifest is deployed.
#     When K8s pod state is running
#     And pod network is not exposed for external connectivity
#     Then netcat connectivity test fails
#     Examples:
#     | manifest |
#     | k8srunner-bdd-timeout.yaml |