@skip
Feature: RECV Component
Scenario Outline: Deploy pod with a valid configuration
    Given I/O performance test configuration is defined in <manifest>.
    When I/O performance test is to be deployed
    Then Helm renders the I/O performance test config and deploys all instances
    And I/O performance statistics are collected and stored for later analysis

    Examples:
    | manifest |
    | k8srunner-performance.yaml |