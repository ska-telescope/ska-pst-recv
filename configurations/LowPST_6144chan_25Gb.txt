HDR_SIZE            4096
HDR_VERSION         1.0

# Fixed Parameters, common to SKALow
TELESCOPE           SKALow
RECEIVER            LFAA
INSTRUMENT          LowCBF
NBIT                16
NANT                1
NPOL                2
NDIM                2
TSAMP               207.36
BAND                Low
WT_NSAMP            32
UDP_NSAMP           32
UDP_NCHAN           24
UDP_FORMAT          LowPST
OS_FACTOR           4/3

# Fixed Parameters, LowPST
FREQ                51.19357639
BW                  22.22222222
NCHAN               6144
START_CHANNEL       0
END_CHANNEL         6144
RESOLUTION          1572864
BYTES_PER_SECOND    237037037.03703704

# Connection Parameters
DATA_HOST           192.168.0.100
DATA_PORT           9510
LOCAL_HOST          192.168.0.100

# Dynamic Parameters, source specific
CALFREQ             11.1111111111
OBS_OFFSET          0
SOURCE              J0437-4715
