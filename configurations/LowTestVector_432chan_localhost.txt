HDR_SIZE            4096
HDR_VERSION         1.0

# Fixed Parameters, common to SKALow
TELESCOPE           SKALow
RECEIVER            LFAA
INSTRUMENT          LowCBF
NBIT                16
NANT                1
NPOL                2
NDIM                2
TSAMP               207.36
BAND                Low
WT_NSAMP            32
UDP_NSAMP           32
UDP_NCHAN           24
UDP_FORMAT          LowTestVector
OS_FACTOR           4/3

# Fixed Parameters, LowTestVector
NCHAN               432
FREQ                51.19357639
BW                  23.98726852
START_CHANNEL       0
END_CHANNEL         432
RESOLUTION          110592
BYTES_PER_SECOND    16666666

# Connection Parameters
DATA_HOST           127.0.0.1
DATA_PORT           9510
LOCAL_HOST          127.0.0.1

# Dynamic Parameters, source specific
CALFREQ             11.1111111111
OBS_OFFSET          0
SOURCE              J0437-4715

DATA_KEY            a000
WEIGHTS_KEY         a010
NUMA_NODE           0

# common header ring buffer definition
HB_NBUFS            8
HB_BUFSZ            4096

# data ring buffer definition (RESOLUTION * 16)
DB_NBUFS            8
DB_BUFSZ            1769472

# weights ring buffer definition (WEIGHTS_RESOLUTION * 16)
WB_NBUFS            8
WB_BUFSZ            14976