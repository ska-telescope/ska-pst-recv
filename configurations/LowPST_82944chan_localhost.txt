HDR_SIZE            4096
HDR_VERSION         1.0

# Fixed Parameters, common to SKALow
TELESCOPE           SKALow
RECEIVER            LFAA
INSTRUMENT          LowCBF
NBIT                16
NANT                1
NPOL                2
NDIM                2
TSAMP               207.36
BAND                Low
WT_NSAMP            32
UDP_NSAMP           32
UDP_NCHAN           24
UDP_FORMAT          LowPST
OS_FACTOR           4/3

# Fixed Parameters, LowPST, whole band
FREQ                150
BW                  300
NCHAN               82944
START_CHANNEL       0
END_CHANNEL         82944
RESOLUTION          21233664
BYTES_PER_SECOND    3200000000

# Note expected buffer size should be 
# DATA_BUFSZ: 169869312
# WEIGHTS_BUFSZ: 1327104

# Connection Parameters
DATA_HOST           127.0.0.1
DATA_PORT           9510
LOCAL_HOST          127.0.0.1

# Dynamic Parameters, source specific
CALFREQ             11.1111111111
OBS_OFFSET          0
SOURCE              J0437-4715

DATA_KEY            a000
WEIGHTS_KEY         a002