PROJECT = ska-pst-recv

DOCS_SOURCEDIR=./docs/src

# include OCI support
include .make/oci.mk

# include cpp make support
include .make/cpp.mk

# include helm make support
include .make/helm.mk

# include k8s make support
include .make/k8s.mk

# include core make support
include .make/base.mk

# TEST: common pst makefile library
include .pst/base.mk

DEV_IMAGE=registry.gitlab.com/ska-telescope/pst/ska-pst-smrb/ska-pst-smrb-builder
DEV_TAG=0.10.6
PROJECT = ska-pst-recv
PROCESSOR_COUNT=${nproc}
OCI_IMAGE_BUILD_CONTEXT=$(PWD)

DOCS_SOURCEDIR=./docs/src

SOURCE_PATH=$(PWD)
BUILD_PATH=$(SOURCE_PATH)/build

PIV_COMMAND=/opt/bin/ska_pst_recv_info
.DEFAULT_GOAL := help


# Default k8s make parameters
# K8S_VALUES=k8srunner
# K8S_CHART_PARAMS=--values=tests/integration/k8s-test/$(K8S_VALUES).yaml --set=image.repository=registry.gitlab.com/ska-telescope/pst/ska-pst-recv/ska-pst-recv

# Variables populated for local development and oci build.
# 	Overriden by CI variables. See .gitlab-cy.yml#L7
PST_SMRB_OCI_REGISTRY?=registry.gitlab.com/ska-telescope/pst/ska-pst-smrb
RECV_BUILDER_IMAGE=${PST_SMRB_OCI_REGISTRY}/ska-pst-smrb-builder
SMRB_RUNTIME_IMAGE=${PST_SMRB_OCI_REGISTRY}/ska-pst-smrb
RECV_RUNTIME_IMAGE=ubuntu:22.04
# This should be initialised in PrivateRules.mak
PST_SMRB_OCI_COMMON_TAG?=${DEV_TAG}
OCI_BUILD_ADDITIONAL_ARGS=--build-arg SMRB_RUNTIME_IMAGE=${SMRB_RUNTIME_IMAGE}:${PST_SMRB_OCI_COMMON_TAG} --build-arg RECV_BUILDER_IMAGE=${RECV_BUILDER_IMAGE}:${PST_SMRB_OCI_COMMON_TAG} --build-arg RECV_RUNTIME_IMAGE=${RECV_RUNTIME_IMAGE}

# define private overrides for above variables in here
-include PrivateRules.mak

# CPP OVERRIDES - Used for builder image
.PHONY: .common.lint .common.artifact-builder
.common.lint:
	apt-get update -y
	apt-get install -y `cat dependencies/builder.apt.txt`
	pip3 install -r dependencies/builder.pip.txt

.common.artifact-builder:
	apt-get update -y
	apt-get install -y build-essential `cat dependencies/builder.apt.txt`

.common.artifact-retriever:
	apt-get update -y
	apt install -y build-essential `cat dependencies/builder.apt.txt`

.PHONY: ci_simulation_performance ci_simulation_bdd_k8s_install ci_simulation_bdd_k8s_logs ci_simulation_bdd_k8s_clean

ci_simulation_bdd_k8s_install:
	@K8S_CHART_PARAMS="$(K8S_CHART_PARAMS)" $(MAKE) k8s-install-chart

ci_simulation_bdd_k8s_logs:
	$(MAKE) k8s-podlogs

ci_simulation_bdd_k8s_clean:
	$(MAKE) k8s-uninstall-chart

ci_simulation_performance: ci_simulation_bdd_k8s_install ci_simulation_bdd_k8s_logs ci_simulation_bdd_k8s_clean

.PHONY: bdd_pytest_performance bdd_pytest_fail
# Pytest prerequisites
# 	apt update -y
# 	apt install -y python3 python3-pip
# 	pip3 install pytest pytest_bdd
bdd_pytest_performance:
	pytest -s -rpP -vvv tests/bdd/k8srunner-performance.py

bdd_pytest_fail:
	pytest -s -rpP -vvv tests/bdd/k8srunner-fail.py

bdd_pytest_fail_wait:
	kubectl -n $(KUBE_NAMESPACE) --for jsonpath='{.status.phase}'=Running --timeout=360s pod -l bdd-test-component=core

bdd_pytest_fail_getpods:
	kubectl -n $(KUBE_NAMESPACE) get pod -l bdd-test-component=core

bdd_pytest_fail_logs:
	kubectl -n $(KUBE_NAMESPACE) logs --all-containers=true -l bdd-test-component=core --tail=-1

_VENV=.venv
_REQUIREMENTS=resources/k8s-bdd/requirements/k8s-bdd.python.txt
.PHONY: local-init-venv local-test-k8slib
BDD_K8S_CMD=pytest -v -spP -o bdd_features_base_dir=$(PWD)/tests/bdd/ --junitxml=./build/k8sbdd.xml resources/k8s-bdd/src/bdd/k8stest.py
local-test-bdd:
	$(call venv_exec,$(_VENV),$(BDD_K8S_CMD))

local-init-venv:
	$(call venv_exec,$(_VENV),pip install -r $(_REQUIREMENTS))

# VENV FUNCTIONS
define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef

# Extend pipeline machinery targets
.PHONY: docs-pre-build
docs-pre-build:
	@rm -rf docs/build/*
	apt-get update -y
	apt-get install -y doxygen fontconfig-config
	pip3 install -r docs/requirements.txt

PROTOBUF_DIR=$(PWD)/protobuf
GENERATED_PATH=$(PWD)/generated

local_generate_code:
	@echo "Generating Python gRPC/Protobuf code."
	@echo "PROTOBUF_DIR=$(PROTOBUF_DIR)"
	@echo "GENERATED_PATH=$(GENERATED_PATH)"
	@echo
	@echo "List of protobuf files: $(shell find "$(PROTOBUF_DIR)" -iname "*.proto")"
	@echo
	@echo "Ensuring generated path $(GENERATED_PATH) exists"
	mkdir -p $(GENERATED_PATH)
	@echo
	protoc --proto_path="$(PROTOBUF_DIR)" \
		--cpp_out="$(GENERATED_PATH)" \
		--grpc_cpp_out="$(GENERATED_PATH)" \
		--plugin=protoc-gen-grpc_cpp=$(shell which grpc_cpp_plugin) \
		$(shell find -L "$(PROTOBUF_DIR)" -iname "*.proto")
	@echo
	@echo "Files generated. $(shell find "$(GENERATED_PATH)/" -iname "*.py")"

.PHONY: local_generate_code
