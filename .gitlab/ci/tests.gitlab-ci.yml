test_compiled_binary_runtime:
  extends: .common.artifact-retriever
  variables:
    OCI_TEST_IMAGE: "registry.gitlab.com/ska-telescope/pst/ska-pst-recv/ska-pst-recv"
    GIT_SUBMODULE_STRATEGY: normal
  stage: test
  when: manual
  needs:
    - build_release
    - oci-image-build
  image: $OCI_TEST_IMAGE:$OCI_TEST_TAG
  before_script:
    - dada_db -k a000 -b 110592 -n 32 -p
    - dada_db -k a002 -b 864 -n 32 -p
    - mkdir -p /mnt/data /mnt/weights
    - |- # write generic config
      cat << EOF >> config.txt
      HDR_SIZE            4096
      HDR_VERSION         1.0

      # Fixed Parameters, common to SKALow
      TELESCOPE           SKALow
      RECEIVER            LFAA
      INSTRUMENT          LowCBF
      NBIT                16
      NANT                1
      NPOL                2
      NDIM                2
      TSAMP               207.36
      BAND                Low
      WT_NSAMP            32
      UDP_NSAMP           32
      UDP_NCHAN           24

      # Fixed Parameters, LowTestVector
      NCHAN               432
      FREQ                51.19357639
      BW                  23.98726852
      START_CHANNEL       0
      END_CHANNEL         432
      RESOLUTION          110592
      BYTES_PER_SECOND    16666666

      # Connection Parameters
      DATA_HOST           127.0.0.1
      DATA_PORT           9510
      LOCAL_HOST          127.0.0.1

      # Dynamic Parameters, source specific
      CALFREQ             11.1111111111
      OBS_OFFSET          0
      SOURCE              J0437-4715

      # Data Block Parameters
      DATA_KEY            a000
      WEIGHTS_KEY         a002
      UDP_FORMAT          LowTestVector

      EOF
  script:
    - stat /mnt/data /mnt/weights ./config.txt
    - dada_dbdisk -k a000 -D /mnt/data -s -d
    - dada_dbdisk -k a002 -D /mnt/weights -s -d
    - ska_pst_recv_udpdb -f ./config.txt > build/ska_pst_recv_udpdb.log 2>&1 &
    - stat /mnt/data /mnt/weights ./config.txt
    - ska_pst_recv_udpgen ./config.txt
  after_script:
    - cat ./config.txt
    - cat build/ska_pst_recv_udpdb.log
  artifacts:
    paths:
      - build/

k8s_interrogate:
  tags:
    - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  when: manual
  before_script:
    - kubectl config get-contexts
    - kubectl auth can-i --list=true -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA
    - kubectl -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA get all
    - kubectl -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA get nodes
    - kubectl -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA describe nodes
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
  script:
    - make k8s-vars
    - make k8s-get-pods
    - make k8s-pod-versions
    - make k8s-describe
    - kubectl logs -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA -l app=ska-pst-recv --all-containers=true

bdd_pytest_performance:
  tags:
  - $K8S_TEST_CLUSTER_TAG
  stage: test
  when: manual
  dependencies:
    - helm-chart-build
  needs:
    - helm-chart-build
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_VALUES: k8srunner-performance
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/$K8S_VALUES.yaml --set image.repository=registry.gitlab.com/ska-telescope/pst/ska-pst-recv/ska-pst-recv --set image.tag=`grep -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"    
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE  
  before_script:
    - apt update -y
    - apt install -y python3 python3-pip
    - pip3 install pytest pytest_bdd
    - kubectl config get-contexts
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - kubectl -n $KUBE_NAMESPACE get nodes
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'  
  script:
    - make k8s-vars
    - mkdir -p build
    - make k8s-template-chart
    # - make k8s-install-chart
    # - kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test-component=udpgen
    - make bdd_pytest_performance
    - make k8s-podlogs
    - kubectl -n $KUBE_NAMESPACE get pods -o yaml | grep hostIP
    - make k8s-uninstall-chart
    - kubectl -n $$KUBE_NAMESPACE get all
    - kubectl delete ns $KUBE_NAMESPACE
    - kubectl get ns

bdd_pytest_fail:
  tags:
  - $K8S_TEST_CLUSTER_TAG
  stage: test
  when: manual
  dependencies:
    - bdd_pytest_performance
  needs:
    - bdd_pytest_performance
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_VALUES: k8srunner-fail
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/$K8S_VALUES.yaml --set image.repository=registry.gitlab.com/ska-telescope/pst/ska-pst-recv/ska-pst-recv --set image.tag=`grep -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"    
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE  
  before_script:
    - apt update -y
    - apt install -y python3 python3-pip
    - pip3 install pytest pytest_bdd
    - kubectl config get-contexts
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - kubectl -n $KUBE_NAMESPACE get nodes
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'  
  script:
    - make k8s-vars
    - mkdir -p build
    # - make k8s-template-chart
    # - make k8s-install-chart
    # - kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test-component=udpgen
    - make bdd_pytest_fail
    - make k8s-podlogs
    - kubectl -n $KUBE_NAMESPACE get pods -o yaml | grep hostIP
    - make k8s-uninstall-chart
    - kubectl -n $$KUBE_NAMESPACE get all
    - kubectl delete ns $KUBE_NAMESPACE
    - kubectl get ns

test_k8s_bdd_recv:
  tags:
  - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    PROJECT: ska-pst-recv
    K8S_CHART_VALUES: k8srunner.yaml
    K8S_CHART_VALUES_PATH: tests/integration
    K8S_CHART_PARAMS: 'test --values=tests/bdd/${K8S_CHART_VALUES}'
    K8S_CHART_PATH: charts/${PROJECT}
    K8S_FEATURES_PATH: tests/bdd/features
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
  dependencies:
    - k8s-test
  needs:
    - k8s-test
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  when: manual
  before_script:
    - apt update -y
    - apt install -y python3 python3-pip python3-venv
    - pip3 install --upgrade pip
    - make local-init-venv
    - mkdir -p build
  script:
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - kubectl config set-context --current --namespace=$KUBE_NAMESPACE
    - make local-test-bdd
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build/
    when: always
  rules:
  - exists:
    - tests/**/*

test_k8s_raw_invalidpacket_tests:
  tags:
    - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/invalidpackets.bdd.yaml --set image.tag=`grep -m 1 -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"
    CI_K8S_WAIT: kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test-component=recv-udprecv
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  when: manual
  dependencies:
    - helm-chart-build
  needs:
    - helm-chart-build
  before_script:
    - kubectl config get-contexts
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get pods
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
    - make k8s-vars
    - make k8s-template-chart
    - make k8s-install-chart
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-get-pods
    - echo "$CI_K8S_WAIT" && $CI_K8S_WAIT
  script:
    # - kubectl config get-contexts
    - mkdir -p build
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-vars
    - make k8s-get-pods
    - make k8s-pod-versions
    - make k8s-describe
    - make k8s-podlogs
    - make k8s-vars >> build/$KUBE_NAMESPACE.log
    - make k8s-get-pods >> build/$KUBE_NAMESPACE.log
    - make k8s-pod-versions >> build/$KUBE_NAMESPACE.log
    - make k8s-describe >> build/$KUBE_NAMESPACE.log
    - make k8s-podlogs >> build/$KUBE_NAMESPACE.log
    - cat build/$KUBE_NAMESPACE.log
    - mkdir -p build_k8s_test
    - mv build build_k8s_test/test_k8s_raw_connection_tests
    - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build_k8s_test/
  rules:
  - exists:
    - tests/**/*

test_k8s_raw_connection_tests:
  tags:
    - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/connection.bdd.yaml --set image.tag=`grep -m 1 -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"
    CI_K8S_WAIT: kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test=connection
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  when: manual
  dependencies:
    - helm-chart-build
  needs:
    - helm-chart-build
  before_script:
    - kubectl config get-contexts
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get pods
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
    - make k8s-vars
    - make k8s-template-chart
    - make k8s-install-chart
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-get-pods
    - echo "$CI_K8S_WAIT" && $CI_K8S_WAIT
  script:
    # - kubectl config get-contexts
    - mkdir -p build
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-vars
    - make k8s-get-pods
    - make k8s-pod-versions
    - make k8s-describe
    - make k8s-podlogs
    - make k8s-vars >> build/$KUBE_NAMESPACE.log
    - make k8s-get-pods >> build/$KUBE_NAMESPACE.log
    - make k8s-pod-versions >> build/$KUBE_NAMESPACE.log
    - make k8s-describe >> build/$KUBE_NAMESPACE.log
    - make k8s-podlogs >> build/$KUBE_NAMESPACE.log
    - cat build/$KUBE_NAMESPACE.log
    - mkdir -p build_k8s_test
    - mv build build_k8s_test/test_k8s_raw_connection_tests
    - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build_k8s_test/
  rules:
  - exists:
    - tests/**/*
  
test_k8s_raw_performance_tests:
  tags:
    - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/performance.bdd.yaml --set image.tag=`grep -m 1 -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"
    CI_K8S_WAIT: kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test=performance
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  when: manual
  dependencies:
    - helm-chart-build
  needs:
    - helm-chart-build
  before_script:
    - kubectl config get-contexts
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get pods
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
    - make k8s-vars
    - make k8s-template-chart
    - make k8s-install-chart
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-get-pods
    - echo "$CI_K8S_WAIT" && $CI_K8S_WAIT
  script:
    # - kubectl config get-contexts
    - mkdir -p build
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-vars
    - make k8s-get-pods
    - make k8s-pod-versions
    - make k8s-describe
    - make k8s-podlogs
    - make k8s-vars >> build/$KUBE_NAMESPACE.log
    - make k8s-get-pods >> build/$KUBE_NAMESPACE.log
    - make k8s-pod-versions >> build/$KUBE_NAMESPACE.log
    - make k8s-describe >> build/$KUBE_NAMESPACE.log
    - make k8s-podlogs >> build/$KUBE_NAMESPACE.log
    - cat build/$KUBE_NAMESPACE.log
    - mkdir -p build_k8s_test
    - mv build build_k8s_test/test_k8s_raw_connection_tests
    - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build_k8s_test/
  rules:
  - exists:
    - tests/**/*