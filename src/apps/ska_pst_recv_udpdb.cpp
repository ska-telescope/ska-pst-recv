/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/recv/config.h"
#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/clients/UDPReceiveDB.h"
#include "ska/pst/recv/network/UDPSocketReceive.h"

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
#include "ska/pst/common/lmc/LmcService.h"
#include "ska/pst/recv/lmc/RecvLmcServiceHandler.h"
#endif

#ifdef HAVE_IBV
#include "ska/pst/recv/network/IBVQueue.h"
#endif

void usage();
void signal_handler(int signal_value);
char quit_threads = 0; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  std::string config_file{};

  int control_port = -1;

  int udp_port = -1;

  std::string service_name = "RECV";

  bool exit_on_packet_receive_timeout = false;

  char verbose = 0;

#ifdef HAVE_IBV
  bool use_ibv = false;
#endif

  int timeout = 0;

  opterr = 0;
  int c = 0;

#ifdef HAVE_IBV
  while ((c = getopt(argc, argv, "c:n:f:iehp:v")) != EOF)
#else
  while ((c = getopt(argc, argv, "c:n:f:ehp:v")) != EOF)
#endif
  {
    switch(c)
    {
      case 'c':
        control_port = atoi(optarg);
        break;

      case 'n':
        service_name = std::string(optarg);
        break;

      case 'f':
        config_file = std::string(optarg);
        break;

#ifdef HAVE_IBV
      case 'i':
        use_ibv = true;
        break;
#endif

      case 'e':
        exit_on_packet_receive_timeout = true;
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'p':
        udp_port = atoi(optarg);
        break;

      case 'v':
        verbose++;
        break;

      default:
        SPDLOG_ERROR("unrecognised option: -{}", static_cast<char>(optopt));
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    SPDLOG_ERROR("ERROR: 1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  std::string recv_ipv4 = std::string(argv[optind]); // NOLINT

  SPDLOG_DEBUG("main: control_port={} recv_ipv4={} config_file={}, udp_port={}", control_port, recv_ipv4, config_file, udp_port);

  // Check that a configuration file or control port are specified
  if ((control_port <= 0) and (config_file.length() == 0))
  {
    SPDLOG_ERROR("ERROR: Either a control_port or config file must be provided");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  std::shared_ptr<ska::pst::recv::SocketReceive> socket{nullptr};
#ifdef HAVE_IBV
  if (use_ibv)
  {
    socket = std::make_shared<ska::pst::recv::IBVQueue>();
  }
  else
#endif
  {
    socket = std::make_shared<ska::pst::recv::UDPSocketReceive>();
  }
  std::shared_ptr<ska::pst::recv::Receiver> recv = std::make_shared<ska::pst::recv::UDPReceiveDB>(socket, recv_ipv4, udp_port);

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  std::shared_ptr<ska::pst::common::LmcService> grpc_control = nullptr;
#endif

  // start the main method
  SPDLOG_DEBUG("Start Receiver::main");
  bool lmc_control = (control_port > 0);

  int return_code = 0;
  try
  {
    if (lmc_control)
    {
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC

      // The ControlledReceiver class implements or inherits the callbacks that will be used by
      // the LmcService:
      //   configure_beam(AsciiHeader)
      //   deconfigure_beam()
      //   configure_scan(AsciiHeader)
      //   deconfigure_beam()
      //   start_scan()
      //   end_scan(AsciiHeader)
      //
      //   The LmcService (or perhaps the upstream component manager) will be
      //   have the responsibility to convert JSON commands into AsciiHeader
      //   keywords as per the PST DDD interface specification.

      SPDLOG_INFO("Setting up gRPC contoller on port {}", control_port);
      auto grpc_handler = std::make_shared<ska::pst::recv::RecvLmcServiceHandler>(recv);
      grpc_control = std::make_shared<ska::pst::common::LmcService>(service_name, grpc_handler, control_port);
      grpc_control->start();
#else
      throw std::runtime_error("gRPC support not compiled");
#endif
    }
    else
    {
      // config file for this data stream
      ska::pst::common::AsciiHeader config;
      SPDLOG_DEBUG("loading configuration from {}", config_file);
      config.load_from_file(config_file);

      ska::pst::common::AsciiHeader header(config);
      header.set_val("SOURCE", "J0437-4715");

      try
      {
        SPDLOG_DEBUG("Configuring Beam");
        recv->configure_beam(config);
        SPDLOG_DEBUG("Configuring Scan");
        recv->configure_scan(header);
        ska::pst::common::AsciiHeader startscan_config;
        startscan_config.set("SCAN_ID", config.get_uint64("SCAN_ID"));
        SPDLOG_DEBUG("Starting the scan with generated SCAN_ID={}", config.get_uint64("SCAN_ID"));
        recv->start_scan(startscan_config);
      }
      catch(const std::exception& e)
      {
        SPDLOG_WARN("Error during configuration. Error={}", e.what());
        recv->reset();
        return_code = 1;
      }
    }

    bool persist = true;
    const ska::pst::recv::UDPStats & stats = recv->get_stats();
    while (persist && return_code == 0)
    {
      usleep(ska::pst::recv::microseconds_per_decisecond);
      if (!lmc_control)
      {
        if(exit_on_packet_receive_timeout)
        {
          persist &= stats.get_packet_receive_timeouts() == 0;
        }
      }
      persist &= !quit_threads;

      SPDLOG_TRACE("main: state={}", recv->get_state());
    }

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
    if (grpc_control)
    {
      // signal the gRPC server to exit
      SPDLOG_INFO("Stopping gRPC controller");
      grpc_control->stop();
      SPDLOG_TRACE("gRPC contoller has stopped");
    }
#endif
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    SPDLOG_DEBUG("udprecv::quit()");
    recv->reset();
    return_code = 1;
  }

  SPDLOG_DEBUG("Stopping Receiver::main");
  recv->quit();

  SPDLOG_DEBUG("returning {}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_recv_udpdb [options] data_host" << std::endl;
  std::cout << "Use UDP sockets to receive a data stream that matches the format, writing the data" << std::endl;
  std::cout << "samples PSRDADA ring buffers for the data (data_key) and weights (weights_key)." << std::endl;
  std::cout << "Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  data_host   network interface to receive incoming data" << std::endl;
  std::cout << "  -c port     port on which to accept control commands" << std::endl;
  std::cout << "  -n name     service name [default is RECV]" << std::endl;
  std::cout << "  -f config   ascii file containing observation configuration" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
#ifdef HAVE_IBV
  std::cout << "  -i          use Infiniband Verbs for socket receive [default UDP socket]" << std::endl;
#endif
  std::cout << "  -e          exit if packet receive timeout occurs" << std::endl;
  std::cout << "  -p port     Override the port on which to receive data [default DATA_PORT]" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (quit_threads)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
}
