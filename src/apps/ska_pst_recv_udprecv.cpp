/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/recv/config.h"
#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/clients/UDPReceiver.h"
#include "ska/pst/recv/network/UDPSocketReceive.h"

#ifdef HAVE_IBV
#include "ska/pst/recv/network/IBVQueue.h"
#endif

void usage();
void signal_handler(int signal_value);
char quit_threads = 0; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  int timeout = 0;

  char verbose = 0;

  bool exit_on_packet_receive_timeout = false;

#ifdef HAVE_IBV
  bool use_ibv = false;
#endif

  int udp_port = -1;

  opterr = 0;

  int c = 0;

#ifdef HAVE_IBV
  while ((c = getopt(argc, argv, "hiep:v")) != EOF)
#else
  while ((c = getopt(argc, argv, "hep:v")) != EOF)
#endif
  {
    switch(c)
    {
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

#ifdef HAVE_IBV
      case 'i':
        use_ibv = true;
        break;
#endif

      case 'e':
        exit_on_packet_receive_timeout = true;
        break;

      case 'p':
        udp_port = atoi(optarg);
        break;

      case 'v':
        verbose++;
        break;

      default:
        SPDLOG_ERROR("unrecognised option: -{}", static_cast<char>(optopt));
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  int return_code = 0;

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 2)
  {
    SPDLOG_ERROR("ERROR: 2 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  SPDLOG_DEBUG("argc={} optind={}", argc, optind);
  for(int i = 0; i < argc; ++i) {
    SPDLOG_DEBUG("i={}, argv[i]={}", i, argv[i]); // NOLINT
  }
  std::string recv_ipv4 = std::string(argv[optind]); // NOLINT
  std::string config_file(argv[optind+1]); // NOLINT
  std::shared_ptr<ska::pst::recv::SocketReceive> socket{nullptr};
#ifdef HAVE_IBV
  if (use_ibv)
  {
    socket = std::make_shared<ska::pst::recv::IBVQueue>();
  }
  else
#endif
  {
    socket = std::make_shared<ska::pst::recv::UDPSocketReceive>();
  }

  // construct a UDPReceiver using the socket
  ska::pst::recv::UDPReceiver udprecv(socket, recv_ipv4, udp_port);

  try
  {
    // prepare the config and header
    ska::pst::common::AsciiHeader config;

    // config file for this data stream
    SPDLOG_DEBUG("loading configuration from {}", config_file);
    config.load_from_file(config_file);
    ska::pst::common::AsciiHeader header(config);
    header.set_val("SOURCE", "J0437-4715");

    SPDLOG_DEBUG("Start Receiver::main");

    try
    {
      SPDLOG_DEBUG("Configuring Beam");
      udprecv.configure_beam(config);

      SPDLOG_DEBUG("Configuring Scan");
      udprecv.configure_scan(header);

      uint64_t scan_id{0};
      if(config.has("SCAN_ID"))
      {
        scan_id = config.get_uint64("SCAN_ID");
      }
      else
      {
        scan_id = static_cast<uint64_t>(time(nullptr));
      }

      ska::pst::common::AsciiHeader startscan_config;
      startscan_config.set("SCAN_ID", scan_id);
      SPDLOG_DEBUG("Starting scan with generated SCAN_ID={}", scan_id);
      udprecv.start_scan(startscan_config);

      const ska::pst::recv::UDPStats& stats = udprecv.get_stats();
      bool persist = true;
      while (persist)
      {
        usleep(ska::pst::recv::microseconds_per_decisecond);
        SPDLOG_TRACE("state={}", ska::pst::common::state_names[udprecv.get_state()]);
        if(exit_on_packet_receive_timeout)
        {
          persist &= stats.get_packet_receive_timeouts() == 0;
        }
        persist &= !quit_threads;
        SPDLOG_TRACE("persist={}", persist);
      }
      SPDLOG_DEBUG("Stopping scan");
      udprecv.stop_scan();

      SPDLOG_DEBUG("Deconfiguring Scan");
      udprecv.deconfigure_scan();

      // release beam configuration
      SPDLOG_DEBUG("Deconfiguring Beam");
      udprecv.deconfigure_beam();

      SPDLOG_DEBUG("stats.get_packet_receive_timeouts()={}", stats.get_packet_receive_timeouts());

    }
    catch(const std::exception& e)
    {
      SPDLOG_WARN("Error={}", e.what());
      udprecv.reset();
      return_code = 1;
    }
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    SPDLOG_DEBUG("udprecv::terminate()");
    udprecv.quit();
    return_code = 1;
  }

  SPDLOG_DEBUG("udprecv::stop_main return return_code={}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_recv_udprecv [options] config" << std::endl;
  std::cout << "Use UDP sockets to receive a data stream from ska_pst_recv_udpgen" << std::endl;
  std::cout << "and report the UDP capture statistics. Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  data_host   network interface to receive incoming data" << std::endl;
  std::cout << "  config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
#ifdef HAVE_IBV
  std::cout << "  -i          use Infiniband Verbs for socket receive [default UDP socket]" << std::endl;
#endif
  std::cout << "  -e          exit if packet receive timeout occurs" << std::endl;
  std::cout << "  -p port     Override the port on which to receive data [default DATA_PORT]" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (quit_threads)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
}
