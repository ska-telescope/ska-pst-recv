/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <spdlog/spdlog.h>
#include <vector>

#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/recv/clients/UDPGenerator.h"

void usage();
void signal_handler(int signal_value);
char quit_threads = 0; // NOLINT

static constexpr unsigned default_tobs = 10;
static constexpr double default_rate = 0.5;

std::shared_ptr<ska::pst::recv::UDPGenerator> udpgen; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  const double gbs_to_bytes_per_second = 1e9 / 8;

  ska::pst::common::AsciiHeader config;

  //! IPv4 address to transmit to
  std::string data_host{};

  //! transmission length in seconds
  unsigned tobs = default_tobs;

  //! data rate in Gb/s
  double data_rate = default_rate * gbs_to_bytes_per_second;

  //! the UDP port to transmit to
  int data_port = -1;

  char verbose = 0;

  std::vector<std::string> error_config_strings;

  opterr = 0;
  int c = 0;

  while ((c = getopt(argc, argv, "d:e:hr:p:t:v")) != EOF)
  {
    switch(c)
    {
      case 'd':
        data_host = std::string(optarg);
        break;

      case 'e':
        error_config_strings.emplace_back(std::string(optarg));
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'r':
        data_rate = atof(optarg) * gbs_to_bytes_per_second;
        break;

      case 'p':
        data_port = atoi(optarg);
        break;

      case 't':
        tobs = atoi(optarg);
        break;

      case 'v':
        verbose++;
        break;

      default:
        SPDLOG_ERROR("unrecognised option: -{}", static_cast<char>(optopt));
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    SPDLOG_ERROR("ERROR: 1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);

  // fixed configuration parameters
  std::string config_file(argv[optind]); // NOLINT

  try
  {
    udpgen = std::make_shared<ska::pst::recv::UDPGenerator>(data_host, data_port);

    // Parse error configurations
    for (auto& error_config_string : error_config_strings)
    {
      std::istringstream iss(error_config_string);
      std::string token;
      bool parsed = true;
      if (!std::getline(iss, token, ':')) parsed = false;
      uint32_t packet_number = atoi(token.c_str());
      if (!std::getline(iss, token, ':')) parsed = false;
      ska::pst::recv::FailureType failure_type = ska::pst::recv::get_failure_type(token);
      if (parsed)
      {
        udpgen->add_induced_error(packet_number, failure_type);
      }
    }

    // config file for this data stream
    SPDLOG_DEBUG("loading configuration from {}", config_file);
    config.load_from_file(config_file);

    SPDLOG_DEBUG("configuring UDP Receiver");
    udpgen->configure_beam(config);

    SPDLOG_DEBUG("allocating runtime resources");
    udpgen->configure_scan(config);

    SPDLOG_DEBUG("Starting transmission");
    udpgen->transmit(tobs, static_cast<float>(data_rate));

    quit_threads = 1;
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return -1;
  }

  return 0;
}

void usage()
{
  std::cout << "Usage: ska_pst_recv_udpgen [options] config" << std::endl;
  std::cout << "Use UDP socket to send a data stream and report the UDP transmission statistics." << std::endl;
  std::cout << "Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -d host     Override the destination host for the data stream [default DATA_HOST]" << std::endl;
  std::cout << "  -e idx:err  Induce an packet generation error (see below) at the specified index (idx)" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -r rate     Data transmission rate in Gb/s [default " << default_rate << "]" << std::endl;
  std::cout << "  -p port     Override the destination port for the data stream [default DATA_PORT]" << std::endl;
  std::cout << "  -t length   Length of data transmission in seconds [default " << default_tobs << "]" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
  std::cout << std::endl;
  std::cout << "Packet generation errors: MagicWord, PacketSize, Transmit, MisorderedPSN, ScanID, ChannelNumber, Timestamp, DataRate" << std::endl;

}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (quit_threads)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
  udpgen->stop_transmit();
}
