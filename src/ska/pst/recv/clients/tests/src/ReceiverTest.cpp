/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/clients/tests/ReceiverTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

void ska::pst::recv::test::ConcreteReceiver::perform_configure_beam()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_configure_beam");
}

void ska::pst::recv::test::ConcreteReceiver::perform_configure_scan()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_configure_scan");
}

void ska::pst::recv::test::ConcreteReceiver::perform_start_scan()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_start_scan");
  keep_receiving = true;
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_start_scan keep_receiving={}", keep_receiving);
  start_receiving();
}

void ska::pst::recv::test::ConcreteReceiver::perform_scan()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_scan");
  wait_for_state(ska::pst::common::Scanning);
  SPDLOG_DEBUG("ska::pst::recv::test::ConcreteReceiver::perform_scan keep_receiving={}", keep_receiving);
  while (keep_receiving)
  {
      SPDLOG_DEBUG("ska::pst::recv::test::ConcreteReceiver::perform_scan keep_receiving={} usleep=10000", keep_receiving);
      usleep(10000); // NOLINT
  }
  SPDLOG_DEBUG("ska::pst::recv::test::ConcreteReceiver::perform_scan keep_receiving={} usleep=100000", keep_receiving);
  usleep(microseconds_per_decisecond);
}

void ska::pst::recv::test::ConcreteReceiver::perform_stop_scan()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_stop_scan");
  keep_receiving = false;
  stop_receiving();
}

void ska::pst::recv::test::ConcreteReceiver::perform_deconfigure_scan()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_deconfigure_scan");
}

void ska::pst::recv::test::ConcreteReceiver::perform_deconfigure_beam()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_deconfigure_beam");
}

void ska::pst::recv::test::ConcreteReceiver::perform_reset()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_reset");
}

void ska::pst::recv::test::ConcreteReceiver::perform_terminate()
{
  SPDLOG_TRACE("ska::pst::recv::ConcreteReceiver::perform_terminate");
}

ReceiverTest::ReceiverTest()
    : ::testing::Test(), _scan_id(static_cast<uint64_t>(time(nullptr)))
{
}


void ReceiverTest::set_config()
{
  beam_config.load_from_file(test_data_file("receiver_beam_config.txt"));
  scan_config.load_from_file(test_data_file("receiver_scan_config.txt"));
  startscan_config.load_from_file(test_data_file("receiver_startscan_config.txt"));
}

void ReceiverTest::SetUp()
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::SetUp"); // NOLINT
  set_config();
  const std::string& data_host = beam_config.get_val("DATA_HOST");
  recv = std::make_shared<ConcreteReceiver>(data_host); // NOLINT

}

void ReceiverTest::TearDown()
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::TearDown"); // NOLINT
  recv->quit(); // NOLINT
  recv = nullptr; // NOLINT
}

auto ReceiverTest::generate_scan_id() -> uint64_t
{
  _scan_id++;
  return _scan_id;
}


TEST_F(ReceiverTest, test_validate_configuration_methods) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_validate_configuration_methods"); // NOLINT
  EXPECT_NO_THROW(recv->configure_beam(beam_config)); // NOLINT
  EXPECT_NO_THROW(recv->configure_scan(scan_config)); // NOLINT
  EXPECT_NO_THROW(recv->start_scan(startscan_config)); // NOLINT
}

TEST_F(ReceiverTest, test_happy_path) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_happy_path"); // NOLINT

  // build complete config
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
  // Configure Beam
  recv->configure_beam(beam_config);
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT

  recv->configure_scan(scan_config);
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  recv->start_scan(startscan_config);
  ASSERT_EQ(ska::pst::common::State::Scanning, recv->get_state()); // NOLINT

  recv->stop_scan();
  ASSERT_EQ(ska::pst::common::State::ScanConfigured, recv->get_state()); // NOLINT

  recv->deconfigure_scan();
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT

  recv->deconfigure_beam();
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  recv->quit();
  ASSERT_EQ(ska::pst::common::State::Unknown, recv->get_state()); // NOLINT
}

TEST_F(ReceiverTest, test_accounting_meta) // NOLINT
{
  db_accounting_t acc = INIT_DB_ACCOUNTING_T;
  static constexpr uint64_t bufsz = 1024;
  acc.bufsz = bufsz;

  ASSERT_EQ(acc.curr_buf, nullptr);
  ASSERT_EQ(acc.next_buf, nullptr);

  ska::pst::recv::Receiver::reset_accounting_meta(&acc);
  ASSERT_EQ(acc.curr_byte_offset, 0);
  ASSERT_EQ(acc.next_byte_offset, bufsz);
  ASSERT_EQ(acc.last_byte_offset, 2 * bufsz);
  ASSERT_EQ(acc.bytes_curr_buf, 0);
  ASSERT_EQ(acc.bytes_next_buf, 0);

  static constexpr uint64_t bytes_curr_buf = 1;
  static constexpr uint64_t bytes_next_buf = 2;

  acc.bytes_curr_buf = bytes_curr_buf;
  acc.bytes_next_buf = bytes_next_buf;

  ska::pst::recv::Receiver::rotate_accounting_meta(&acc);
  ASSERT_EQ(acc.curr_byte_offset, bufsz);
  ASSERT_EQ(acc.next_byte_offset, 2 * bufsz);
  ASSERT_EQ(acc.last_byte_offset, 3 * bufsz);
  ASSERT_EQ(acc.bytes_curr_buf, bytes_next_buf);
  ASSERT_EQ(acc.bytes_next_buf, 0);
}

TEST_F(ReceiverTest, test_accounting_buffers) // NOLINT
{
  db_accounting_t acc = INIT_DB_ACCOUNTING_T;
  static constexpr uint64_t bufsz = 1024;
  acc.bufsz = bufsz;

  std::vector<char> buffer_a(bufsz);
  std::vector<char> buffer_b(bufsz);

  acc.curr_buf = &buffer_a[0];
  acc.next_buf = &buffer_b[0];

  ska::pst::recv::Receiver::rotate_accounting_buffers(&acc);

  ASSERT_EQ(acc.curr_buf, &buffer_b[0]);
  ASSERT_EQ(acc.next_buf, &buffer_a[0]);

 ska::pst::recv::Receiver::advance_accounting_buffers(&acc);

  ASSERT_EQ(acc.curr_buf, &buffer_a[0]);
  ASSERT_EQ(acc.next_buf, nullptr);
}

TEST_F(ReceiverTest, test_validate_configure_beam_missing_key) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_validate_configure_beam_missing_key"); // NOLINT

  beam_config.del("DATA_HOST");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
}

TEST_F(ReceiverTest, test_validate_configure_beam_ipv4_pattern) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_validate_configure_beam_ipv4_pattern"); // NOLINT

  beam_config.set_val("DATA_HOST","256.0.0.1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("DATA_HOST","192.192.0.0.1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("DATA_HOST","192.192.192.");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("DATA_HOST",".1.1.1.1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("DATA_HOST","255.1.1.1.");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("DATA_HOST","a.b.c.d");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

}

TEST_F(ReceiverTest, test_validate_configure_beam_number_patterns) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_validate_configure_beam_number_patterns"); // NOLINT

  // invalid ^[\\d]{1,}$
  beam_config.set_val("NCHAN","-1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("NCHAN","1.1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("NCHAN","a");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("NCHAN",".1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("NCHAN","a");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT



  set_config();

  // invalid ^[\\d]{1,}.[\\d]{1,}$
  beam_config.set_val("TSAMP","1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("TSAMP","-1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("TSAMP","a");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("TSAMP"," . ");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT
  beam_config.set_val("TSAMP"," 0.0 ");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

  beam_config.set_val("TSAMP","-1.1");
  EXPECT_THROW(recv->configure_beam(beam_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::Idle, recv->get_state()); // NOLINT

}

TEST_F(ReceiverTest, test_validate_configure_scan_missing_key) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_validate_configure_scan_missing_key"); // NOLINT

  // configure beam
  recv->configure_beam(beam_config);
  EXPECT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT


  scan_config.del("SOURCE");
  EXPECT_THROW(recv->configure_scan(scan_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
}

TEST_F(ReceiverTest, test_validate_configure_scan_value_patterns) // NOLINT
{
  SPDLOG_TRACE("ska::pst::recv::test::ReceiverTest::test_validate_configure_scan_value_patterns"); // NOLINT

  // configure beam
  recv->configure_beam(beam_config);
  EXPECT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT


  scan_config.set_val("SOURCE","j - ");
  EXPECT_THROW(recv->configure_scan(scan_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  scan_config.set_val("SOURCE","j 1234-1234 ");
  EXPECT_THROW(recv->configure_scan(scan_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  scan_config.set_val("SOURCE","j1234 - 1234");
  EXPECT_THROW(recv->configure_scan(scan_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  scan_config.set_val("SOURCE","j 1234 -1234");
  EXPECT_THROW(recv->configure_scan(scan_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
  scan_config.set_val("SOURCE","j1234-1234-1234");
  EXPECT_THROW(recv->configure_scan(scan_config), ska::pst::common::pst_validation_error); // NOLINT
  ASSERT_EQ(ska::pst::common::State::BeamConfigured, recv->get_state()); // NOLINT
}

} // namespace ska::pst::recv::test