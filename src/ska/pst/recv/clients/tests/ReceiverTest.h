/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <gtest/gtest.h>

#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/recv/clients/Receiver.h"

#ifndef SKA_PST_RECV_TESTS_ReceiverTest_h
#define SKA_PST_RECV_TESTS_ReceiverTest_h

namespace ska::pst::recv::test {


class ConcreteReceiver : public Receiver
{
    private:
        bool keep_receiving{false};

    protected:

    public:
        ConcreteReceiver(const std::string& _data_host) : Receiver(_data_host) {
            beam_config_value_patterns = {
                { "DATA_HOST", "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$" },
                { "NCHAN", "^[\\d]{1,}$" },
                { "NBIT", "^[\\d]{1,}$" },
                { "NPOL", "^[\\d]{1,}$" },
                { "NDIM", "^[\\d]{1,}$" },
                { "TSAMP", "^[\\d]{1,}.[\\d]{1,}$" },
                { "BW", "^[\\d]{1,}.[\\d]{1,}$" },
                { "FREQ", "^[\\d]{1,}$" },
                { "START_CHANNEL", "^[\\d]{1,}$" },
                { "END_CHANNEL", "^[\\d]{1,}$" }
            };
            scan_config_value_patterns = {
                { "SOURCE", "^J[\\d]{4}-[\\d]{4}$" }
            };
            start_scan_config_value_patterns = {
                { "SCAN_ID", "^[\\d]{1,}$" }
            };
            wait_for_state(ska::pst::common::State::Idle);
        };
        ~ConcreteReceiver() = default;
        std::string data_host{};

        uint64_t get_data_bufsz() { return 0; };

        // Resources
        void perform_configure_beam();
        void perform_configure_scan();
        void perform_start_scan();
        void perform_scan();
        void perform_stop_scan();
        void perform_deconfigure_scan();
        void perform_deconfigure_beam();
        void perform_reset();
        void perform_terminate();

};

/**
 * @brief Test the Receiver class
 * 
 * @details
 * 
 */
class ReceiverTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

        uint64_t generate_scan_id();

        void set_config();

    public:
        ReceiverTest();

        ~ReceiverTest() = default;

        ska::pst::common::AsciiHeader beam_config;
        ska::pst::common::AsciiHeader scan_config;
        ska::pst::common::AsciiHeader startscan_config;

        std::shared_ptr<ConcreteReceiver> recv{nullptr};

    private:

        uint64_t _scan_id{0};

};

} // namespace ska::pst::recv::test

#endif // SKA_PST_RECV_TESTS_ReceiverTest_h

