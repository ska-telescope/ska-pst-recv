/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <ctime>
#include <regex>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Time.h"
#include "ska/pst/common/utils/AsciiHeader.h"

#include "ska/pst/recv/network/SocketReceive.h"
#include "ska/pst/recv/definitions.h"

#include "ska/pst/recv/clients/Receiver.h"
#include "ska/pst/recv/formats/UDPFormatFactory.h"


ska::pst::recv::Receiver::Receiver(std::string _data_host, int _data_port) : ska::pst::common::ApplicationManager("recv"), data_host(std::move(_data_host)), data_port(_data_port)
{
  SPDLOG_TRACE("ska::pst::recv::Receiver::Receiver data_host={} data_port={}", data_host, data_port);
  initialise();
}

ska::pst::recv::Receiver::Receiver(std::string _data_host) : ska::pst::common::ApplicationManager("recv"), data_host(std::move(_data_host))
{
  SPDLOG_TRACE("ska::pst::recv::Receiver::Receiver data_host={}", data_host);
  initialise();
}

ska::pst::recv::Receiver::~Receiver()
{
  SPDLOG_TRACE("ska::pst::recv::Receiver::~Receiver()");

  // if the monitoring thread was not joined in stop_receiving
  if (monitor_thread)
  {
    monitor_thread->join();
    SPDLOG_TRACE("ska::pst::recv::Receiver::~Receiver monitor_thread joined");
    monitor_thread = nullptr;
  }
}

void ska::pst::recv::Receiver::perform_initialise()
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::perform_initialise");
  data = INIT_DB_ACCOUNTING_T;
  weights = INIT_DB_ACCOUNTING_T;
}

void ska::pst::recv::Receiver::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::validate_configure_beam");
  check_config_value_patterns("beam_config", config, beam_config_value_patterns, context);

  // the check config value patterns check that UDP_FORMAT exists or not
  // this check is to avoid error being thrown if UDP_FORMAT does not
  // exist in the configuration.
  if (config.has("UDP_FORMAT")) {
    std::string format_name = config.get_val("UDP_FORMAT");
    if (!ska::pst::recv::is_format_supported(format_name))
    {
      std::stringstream ss;
      ss << "unknown format value. Supported values are ";
      ss << ska::pst::recv::get_supported_formats_list();
      context->add_validation_error("UDP_FORMAT", format_name, ss.str());
      return;
    }

    auto format = ska::pst::recv::UDPFormatFactory(format_name);
    format->validate_configure_beam(config, context);
  }
}

void ska::pst::recv::Receiver::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::validate_configure_scan");
  check_config_value_patterns("scan_config", config, scan_config_value_patterns, context);
}

void ska::pst::recv::Receiver::validate_start_scan(const ska::pst::common::AsciiHeader& config)
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::validate_start_scan");
  ska::pst::common::ValidationContext context;
  check_config_value_patterns("start_scan_config", config, start_scan_config_value_patterns, &context);
  context.throw_error_if_not_empty();
}

void ska::pst::recv::Receiver::perform_stop_scan()
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::perform_stop_scan calling stop_receiving");
  stop_receiving();
  SPDLOG_DEBUG("ska::pst::recv::Receiver::perform_stop_scan calling print_stats");
  print_stats();
  SPDLOG_DEBUG("ska::pst::recv::Receiver::perform_stop_scan stop_receiving exited");
}

void ska::pst::recv::Receiver::start_receiving()
{
  scan_id = startscan_config.get_uint64("SCAN_ID");
  SPDLOG_DEBUG("ska::pst::recv::Receiver::start_receiving");
  ska::pst::recv::SocketReceive::keep_receiving = true;
}

void ska::pst::recv::Receiver::stop_receiving()
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::stop_receiving");
  ska::pst::recv::SocketReceive::keep_receiving = false;

  if (monitor_thread)
  {
    monitor_thread->join();
    SPDLOG_TRACE("ska::pst::recv::Receiver::stop_receiving monitor_thread joined");
    monitor_thread = nullptr;
  }
  scan_id = UINT64_MAX;
}

void ska::pst::recv::Receiver::monitor_method()
{
  uint64_t report_count = 0;
  static constexpr uint64_t report_header_period = 20;

  SPDLOG_DEBUG("ska::pst::recv::Receiver::monitor_thread running");
  bool monitoring = true;
  while (monitoring)
  {
    // compute the instaneous transmission and drop rates
    stats.compute_rates();

    uint64_t b_drop = stats.get_data_dropped();
    double gb_recv_ps = stats.get_data_transmission_rate() * ska::pst::recv::gigabits_per_byte;
    double gb_drop_ps = stats.get_data_drop_rate() * ska::pst::recv::gigabits_per_byte;
    double sleeps_ps = stats.get_sleep_rate();

    // determine how much memory is free in the receivers
    if (report_count % report_header_period == 0)
    {
      SPDLOG_INFO("Received\tDropped\tTotal Dropped\tSleeps");
    }

    std::ostringstream oss;
    oss << gb_recv_ps << "\t" << gb_drop_ps << "\t" << b_drop << "\t" << sleeps_ps;
    SPDLOG_INFO("{}", oss.str());

    // wait for the state to be anything other than scanning, if the timeout occurs
    monitoring = !wait_for_not_state(ska::pst::common::State::Scanning, milliseconds_per_second);
    SPDLOG_TRACE("ska::pst::recv::Receiver::monitor_thread !wait_for_not_state returned monitoring={} state={}", monitoring, get_name(state));

    monitoring &= ska::pst::recv::SocketReceive::keep_receiving;
    SPDLOG_DEBUG("ska::pst::recv::Receiver::monitor_thread &=keep_receiving returned monitoring={} state={}", monitoring, get_name(state));

    report_count++;
  }
  SPDLOG_DEBUG("ska::pst::recv::Receiver::monitor_thread exiting");
}

void ska::pst::recv::Receiver::parse_beam_config()
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::parse_beam_config");

  uint32_t nant{1}, nbeam{1};
  try { beam_config.get("NANT", &nant); }
  catch (std::exception& exc) { nant = 1; }

  try { beam_config.get("NBEAM", &nbeam); }
  catch (std::exception& exc){ nbeam = 1; }

  nsig = nant * nbeam;

  beam_config.get("NCHAN", &nchan);
  beam_config.get("NBIT", &nbit);
  beam_config.get("NPOL", &npol);
  beam_config.get("NDIM", &ndim);
  beam_config.get("TSAMP", &tsamp);
  beam_config.get("BW", &bw);
  beam_config.get("FREQ",&freq);
  beam_config.get("START_CHANNEL", &start_channel);
  beam_config.get("END_CHANNEL", &end_channel);
  SPDLOG_DEBUG("ska::pst::recv::Receiver::parse_beam_config nsig={} nchan={} nbit={} npol={} ndim={} tsamp={} bw={} freq={}",
                nsig, nchan, nbit, npol, ndim, tsamp, bw, freq);

  auto bits_per_sample = nsig * nchan * npol * ndim * nbit;
  bits_per_second  = (static_cast<double>(bits_per_sample) * ska::pst::recv::microseconds_per_second) / tsamp;
  bytes_per_second = bits_per_second / ska::pst::recv::bits_per_byte;
  SPDLOG_DEBUG("ska::pst::recv::Receiver::parse_beam_config bits_per_second={} bytes_per_second={}", bits_per_second, bytes_per_second);

  if (data_port <= 0)
  {
    beam_config.get("DATA_PORT", &data_port);
  }

  SPDLOG_DEBUG("ska::pst::recv::Receiver::parse_beam_config receiving {}:{}", data_host, data_port);
}

void ska::pst::recv::Receiver::parse_scan_config()
{
  if (!scan_config.has("SOURCE"))
  {
    throw std::runtime_error("SOURCE did not exist in header");
  }
}

void ska::pst::recv::Receiver::set_config_value_patterns(std::map<std::string,std::string> beam_config, std::map<std::string,std::string> scan_config, std::map<std::string,std::string> start_scan_config)
{
  beam_config_value_patterns = std::move(beam_config);
  scan_config_value_patterns = std::move(scan_config);
  start_scan_config_value_patterns = std::move(start_scan_config);
}

void ska::pst::recv::Receiver::check_config_value_patterns(const std::string& config_name, const ska::pst::common::AsciiHeader& config, const std::map<std::string,std::string>& config_value_patterns, ska::pst::common::ValidationContext *context)
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::check_config config_name={}",config_name);
  std::string config_key, config_value_pattern;

  // Loop through keys
  for (const auto &key_value_pair : config_value_patterns)
  {
    config_key = key_value_pair.first;
    config_value_pattern = key_value_pair.second;
    std::regex regex_pattern(key_value_pair.second);

    if (config.has(config_key))
    {
      auto config_value = config.get_val(config_key);
      if (!std::regex_match(config_value, regex_pattern))
      {
        context->add_value_regex_error(config_key, config_value, config_value_pattern);
      }
    }
    else
    {
      context->add_missing_field_error(config_key);
    }
  }
}

void ska::pst::recv::Receiver::print_stats()
{
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_data_transmitted()={}", stats.get_data_transmitted());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_packets_transmitted()={}", stats.get_packets_transmitted());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_data_dropped()={}", stats.get_data_dropped());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_packets_dropped()={}", stats.get_packets_dropped());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_discarded()={}", stats.get_discarded());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_nsleeps()={}", stats.get_nsleeps());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_ignored()={}", stats.get_ignored());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_malformed()={}", stats.get_malformed());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_misdirected()={}", stats.get_misdirected());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_misordered()={}", stats.get_misordered());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_data_transmission_rate()={}", stats.get_data_transmission_rate());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_data_drop_rate()={}", stats.get_data_drop_rate());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_sleep_rate()={}", stats.get_sleep_rate());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_packet_receive_timeouts()={}", stats.get_packet_receive_timeouts());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_packet_timeout_threshold()={}", stats.get_packet_timeout_threshold());
  SPDLOG_DEBUG("ska::pst::recv::Receiver::print_stats: stats.get_initial_packet_timeout_threshold()={}", stats.get_initial_packet_timeout_threshold());
}
