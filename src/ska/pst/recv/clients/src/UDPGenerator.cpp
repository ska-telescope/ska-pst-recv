/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <sys/time.h>
#include <unistd.h>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Time.h"
#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/clients/UDPGenerator.h"
#include "ska/pst/recv/formats/UDPFormatFactory.h"
#include "ska/pst/common/utils/PacketGeneratorFactory.h"

ska::pst::recv::UDPGenerator::UDPGenerator(std::string host, int port) :
  data_host(std::move(host)), data_port(port)
{
}

ska::pst::recv::UDPGenerator::~UDPGenerator()
{
  sock.close_me();
}

void ska::pst::recv::UDPGenerator::add_induced_error(uint32_t packet_number, ska::pst::recv::FailureType failure_type)
{
  induced_errors.emplace_back(std::make_pair(packet_number, failure_type));
}

void ska::pst::recv::UDPGenerator::configure_beam(const ska::pst::common::AsciiHeader &beam_config)
{
  unsigned nant{1}, nbeam{1};

  beam_config.get("BEAM_ID", &beam_id);

  try { beam_config.get("SCAN_ID", &scan_id); }
  catch (std::exception& exc) { scan_id = 1; }

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam scan_id={}", scan_id);

  try { beam_config.get("NANT", &nant); }
  catch (std::exception& exc) { nant = 1; }

  try { beam_config.get("NBEAM", &nbeam); }
  catch (std::exception& exc) { nbeam = 1; }

  beam_config.get("NCHAN", &nchan);
  beam_config.get("NBIT", &nbit);
  beam_config.get("NPOL", &npol);
  beam_config.get("NDIM", &ndim);
  beam_config.get("TSAMP", &tsamp);
  beam_config.get("BW", &bw);

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam parsing host info");
  if (data_host.length() == 0)
  {
    data_host = beam_config.get_val("DATA_HOST");
  }
  if (data_port <= 0)
  {
    beam_config.get("DATA_PORT", &data_port);
  }

  try { local_host = beam_config.get_val("LOCAL_HOST"); }
  catch (std::exception& exc){ local_host = "any"; }

  std::string udp_format = beam_config.get_val("UDP_FORMAT");
  format = ska::pst::recv::UDPFormatFactory(udp_format);

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam configuring beam for format");
  format->configure_beam(beam_config);
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam configuring header");
  udp_header.configure(beam_config, *(format.get()));

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam configuring stats");
  stats.configure(format.get());
  SPDLOG_DEBUG("UDP dest={}:{} <- {}", data_host, data_port, local_host);

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam sock.resize({})", format->get_packet_size());
  sock.resize(format->get_packet_size());
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_beam sock.open({}, {})", data_host, data_port);
  sock.open(data_host, data_port, local_host);

  auto bits_per_sample = nsig * nchan * npol * ndim * nbit;
  bits_per_second  = static_cast<uint64_t>(rint((static_cast<double>(bits_per_sample) * microseconds_per_second) / tsamp));
  bytes_per_second = bits_per_second / ska::pst::recv::bits_per_byte;
}

void ska::pst::recv::UDPGenerator::configure_scan(const ska::pst::common::AsciiHeader &scan_config)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator:prepare format->configure_scan(scan_config)");
  format->configure_scan(scan_config);

  ska::pst::common::Time start_timestamp;
  // ensure the complete header includes a UTC_START
  try
  {
    std::string config_utc_start = scan_config.get_val("UTC_START");
    SPDLOG_INFO("Using configured UTC_START={}", config_utc_start);
    start_timestamp.set_time(config_utc_start.c_str());
    try
    {
      uint64_t picoseconds = scan_config.get_uint64("PICOSECONDS");
      static constexpr uint64_t attoseconds_per_picosecond = 1000;
      start_timestamp.set_fractional_time(picoseconds * attoseconds_per_picosecond);
    }
    catch(const std::exception& e)
    {
      SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_scan did not find PICOSCONDS in scan_config, assuming 0");
    }
  }
  catch(const std::exception& e)
  {
    start_timestamp.set_time(time(nullptr));
    start_timestamp.add_seconds(start_delay);
    SPDLOG_INFO("Generated UTC_START={}", start_timestamp.get_gmtime());
  }

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_scan using UTC_START={}", start_timestamp.get_gmtime());
  udp_header.set_timestamp(start_timestamp);
  udp_header.set_scan_id(scan_id);

  if (!scan_config.has("SOURCE"))
  {
    SPDLOG_ERROR("ska::pst::recv::UDPGenerator::configure_scan SOURCE did not exist in scan configuration");
    throw std::runtime_error("SOURCE did not exist in scan configuration");
  }

  // test for optional specification of packet data generator
  std::string dg_name = "none";
  try { dg_name = scan_config.get_val("DATA_GENERATOR"); }
  catch (std::exception& exc) { dg_name = "none"; }

  if (dg_name != "none" )
  {
    ska::pst::common::AsciiHeader scan_header;
    scan_header.clone(scan_config);
    scan_header.set_val("UTC_START", start_timestamp.get_gmtime());
    scan_header.set("OBS_OFFSET", 0);

    SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_scan format->get_nchan_per_packet()={}", format->get_nchan_per_packet());
    data_generator = ska::pst::common::PacketGeneratorFactory(dg_name, format);
    SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_scan configuring data generator");
    data_generator->configure(scan_header);
    SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_scan data generator configured");
  }

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::configure_scan complete");
}

void ska::pst::recv::UDPGenerator::set_start_packet_sequence_number(uint64_t psn)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::set_start_packet_sequence_number psn={}", psn);
  udp_header.set_packet_sequence_number(psn);
}

void ska::pst::recv::UDPGenerator::transmit(unsigned tobs, float data_rate)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::transmit tobs={} data_rate={} bytes_per_second={}",
    tobs, data_rate, bytes_per_second);

  // if no data rate has been specified, use the bytes per second
  if (data_rate <= 0)
  {
    data_rate = static_cast<float>(bytes_per_second);
  }

  // sort the induced errors by the packet number
  sort(induced_errors.begin(), induced_errors.end());
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::transmit inducing {} errors", induced_errors.size());

  auto bytes_to_send = static_cast<uint64_t>(rintf(data_rate * static_cast<float>(tobs)));
  uint64_t packets_to_send = bytes_to_send / format->get_packet_size();
  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::transmit bytes_to_send={} packets_to_send={}", bytes_to_send, packets_to_send);

  auto packets_per_second = static_cast<uint64_t>(ceilf(data_rate / static_cast<float>(format->get_packet_size())));
  auto sleep_time_us = static_cast<double>(microseconds_per_second) / static_cast<double>(packets_per_second);

  SPDLOG_DEBUG("ska::pst::recv::UDPGenerator::transmit packets_per_second={} sleep_time_us={}", packets_per_second, sleep_time_us);

  char * buf = sock.get_buf();
  uint64_t total_bytes_sent{0};

  struct timeval timestamp{};
  time_t start_second{0};

  gettimeofday(&timestamp, nullptr);
  start_second = timestamp.tv_sec + 1;

  double micro_seconds = 0;
  double micro_seconds_elapsed = 0;

  // busy sleep until next 1pps tick
  while (timestamp.tv_sec < start_second)
  {
    gettimeofday(&timestamp, nullptr);
  }

  char wait = 1;

  const uint64_t payload_size = format->get_packet_data_size();

  keep_transmitting = true;

  // start a transmitting thread
  start_monitor_thread();

  uint32_t pkt_idx = 0;
  uint32_t err_idx = 0;
  size_t pkt_size{};

  while (total_bytes_sent < bytes_to_send && keep_transmitting)
  {
    if (err_idx < induced_errors.size() && pkt_idx == induced_errors[err_idx].first)
    {
      pkt_size = udp_header.gen_packet_failure(buf, induced_errors[err_idx].second);
      err_idx++;
    }
    else
    {
      pkt_size = udp_header.gen_packet(buf);
    }
    pkt_idx++;

    if (data_generator)
    {
      data_generator->fill_packet(buf);
    }

    sock.send(pkt_size);
    stats.increment();

    // determine the desired time to wait un
    micro_seconds += sleep_time_us;

    wait = 1;

    while (wait && keep_transmitting)
    {
      gettimeofday (&timestamp, nullptr);
      micro_seconds_elapsed = (static_cast<double>(timestamp.tv_sec - start_second) * microseconds_per_second) + static_cast<double>(timestamp.tv_usec);

      if (micro_seconds_elapsed > micro_seconds)
      {
        wait = 0;
      }
    }

    total_bytes_sent += payload_size;
  }

  // stop the transmitting thread
  stop_monitor_thread();
}

void ska::pst::recv::UDPGenerator::start_monitor_thread()
{
  keep_transmitting = true;
  monitor_thread = std::make_unique<std::thread>(std::thread(&ska::pst::recv::UDPGenerator::monitor_method, this));
}

void ska::pst::recv::UDPGenerator::stop_monitor_thread()
{
  keep_transmitting = false;
  if (monitor_thread)
  {
    monitor_thread->join();
    monitor_thread = nullptr;
  }
}

auto ska::pst::recv::UDPGenerator::get_stats() -> ska::pst::recv::UDPStats&
{
  return stats;
}

void ska::pst::recv::UDPGenerator::monitor_method()
{
  uint64_t b_sent_total = 0;
  uint64_t b_sent_1sec = 0;
  uint64_t b_sent_curr = 0;
  uint64_t report_count = 0;

  double gb_sent_ps = 0;
  static constexpr unsigned header_report_period = 20; // seconds

  sleep(1);

  while (keep_transmitting)
  {
    // get a snapshot of the data as quickly as possible
    b_sent_curr = stats.get_data_transmitted();

    // calc the values for the last second
    b_sent_1sec = b_sent_curr - b_sent_total;

    // update the totals
    b_sent_total = b_sent_curr;

    gb_sent_ps = static_cast<double>(b_sent_1sec) * ska::pst::recv::gigabits_per_byte;

    if (report_count % header_report_period == 0)
    {
      SPDLOG_INFO("Sent [Gb/s]");
    }
    report_count++;

    SPDLOG_INFO("{}", gb_sent_ps);

    // sleep for 1 second, with a 0.1 second check of keep_transmitting
    int iterations = deciseconds_per_second;
    while (keep_transmitting && iterations > 0)
    {
      usleep(microseconds_per_decisecond);
      iterations--;
    }
  }
}
