/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <netinet/in.h>

#include "ska/pst/recv/network/UDPSocket.h"

#ifndef SKA_PST_RECV_NETWORK_UDPSocketSend_h
#define SKA_PST_RECV_NETWORK_UDPSocketSend_h

namespace ska::pst::recv {

  /**
   * @brief Provides implementation of a UDP socket for transmitting UDP packets.
   * Abstracts much of the complexity of dealing with linux socket libraries.
   *
   */
  class UDPSocketSend : public UDPSocket {

    public:

      /**
       * @brief Construct a new UDPSocketSend object
       *
       */
      UDPSocketSend();

      /**
       * @brief Destroy the UDPSocketSend object
       *
       */
      virtual ~UDPSocketSend() = default;

      //! open the socket to send to the IP address, port, binding to the local IP address
      /**
       * @brief open the socket to send to the IP address, port, binding to the local IP address
       *
       * @param dest_ipv4_address destination IPv4 address for the UDP packets
       * @param dest_port destination port for the UDP packets
       * @param local_ipv4_addr local interface to use for sending to the destination. If "any" is provided use any available interface
       */
      void open(const std::string &dest_ipv4_address, int dest_port, const std::string &local_ipv4_addr);

      /**
       * @brief Send the contents of the buf to as a UDP packet
       *
       * @return size_t number of bytes sent
       */
      size_t send();

      /**
       * @brief Send the contents of the buf, but limit the packet size.
       *
       * @param nbytes number of bytes to send
       * @return size_t number of bytes sent
       */
      size_t send(size_t nbytes);

    private:

      //! sockaddr_in struct for UDP socket
      struct sockaddr * sock_addr;

      //! size of the sockaddr structure
      size_t sock_size;

  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_UDPSocketSend_h
