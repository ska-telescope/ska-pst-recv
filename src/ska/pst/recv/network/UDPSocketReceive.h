/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <vector>
#include <netinet/in.h>

#include "ska/pst/recv/network/UDPSocket.h"
#include "ska/pst/recv/network/SocketReceive.h"

#ifndef SKA_PST_RECV_NETWORK_UDPSocketReceive_h
#define SKA_PST_RECV_NETWORK_UDPSocketReceive_h

namespace ska::pst::recv {

  /**
   * @brief Provides implementation of a standard UDP socket for receiving UDP packets.
   * Abstracts the complexity of interacting with linux socket libraries
   * 
   */
  class UDPSocketReceive : public UDPSocket, public SocketReceive {

    public:

      //! minimum valid size of a UDP packet in bytes
      static constexpr int min_size = 8;

      /**
       * @brief Construct a new UDPSocketReceive object
       * 
       */
      UDPSocketReceive();

      /**
       * @brief Destroy the UDPSocketReceive object
       * 
       */
      ~UDPSocketReceive();

      /**
       * @brief Resize the linux kernel's socket buffer to the specified size.
       * The socket buffer is used to receive packets asynchronously whilst the application is
       * busy on other tasks. Linux kernel restrictions might restrict the maximum size.
       * 
       * @param bufsz new size of the socket buffer
       * @param packet_size size of the UDP payload 
       * @return size_t size of the actual sock buffer allocated
       */
      void allocate_resources(size_t bufsz, size_t packet_size);

      /**
       * @brief Open the UDP receiving socket at the specified endpoint.
       * Opens the UDP socket at the interface associated with the specified address, or if
       * "any" is specified, listen on any interface.
       * 
       * @param ip_address IPv4 address for receiving UDP socke
       * @param port Port number for the UDP socket.
       */
      virtual void configure(const std::string& ip_address, int port);

      /**
       * @brief Release the resources in use by the receiving socket.
       * 
       */
      void deconfigure_beam();

      /**
       * @brief Discard all packets that are waiting in the kernel's socket buffer.
       * 
       * @return size_t number of bytes cleared.
       */
      size_t clear_buffered_packets();

      /**
       * @brief Receive a single UDP packet into the socket buffer.
       * If the previously received packet was not consumed via consume_packet, then no packet is received.
       * If the socket is blocking, this method will block until a packet is received.
       * If the socket is non-blocking, this method will busy-wait until a packet is received.
       * In the latter case, the busy-wait may be interrupted by setting UDPSocketReceiver::keep_receiving to false.
       * 
       * @return ssize_t Number of bytes received
       */
      int acquire_packet();

      /**
       * @brief Marks the most recently received packet as consumed.
       *
       */
      inline void release_packet(int slot) { have_packet = false; };

      /**
       * @brief Return the number of accumulated sleeps, resetting the internal counter to 0.
       * 
       * @return uint64_t Number of accumulated sleeps since last reset.
       */
      uint64_t process_sleeps();

      //! Pointer to the internal socket buffer
      char * buf_ptr{nullptr};

      char * get_buf_ptr(int slot_index);

    protected:

      //! flag for whether bufsz contains a packet
      bool have_packet{false};

      //! Size of the kernel receive buffer in bytes
      size_t kernel_socket_bufsz{0};

    private:

  };

} // namespace ska::pst::recv

#endif
