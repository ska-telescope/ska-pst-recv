/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <inttypes.h>

#include "ska/pst/recv/formats/UDPFormat.h"
#include "ska/pst/common/utils/Timer.h"

#ifndef SKA_PST_RECV_NETWORK_UDPStats_h
#define SKA_PST_RECV_NETWORK_UDPStats_h

namespace ska::pst::recv {

  /**
   * @brief Accumulate statistics for UDP based sockets.
   * Providees information on data capture rates, data loss rates, interface errors.
   *
   */
  class UDPStats {

    public:

      /**
       * @brief Construct a new UDPStats object
       *
       */
      UDPStats();

      /**
       * @brief Destroy the UDPStats object
       *
       */
      ~UDPStats() = default;

      /**
       * @brief Configure the object from a UDPFormat.
       * Extracts the UDPFormat's packet_size and packet_data_size for use in
       * calculating statistics.
       *
       * @param format UDP format to use for configuration
       */
      void configure(UDPFormat * format);

      /**
       * @brief Increment bytes_transmitted by the specified number of packets.
       *
       * @param num_packets The number of packets to incremenet by.
       */
      void increment(uint64_t num_packets=1);

      /**
       * @brief Increment bytes_transmitted by the specified number of bytes.
       *
       * @param nbytes Number of bytes to increment by.
       */
      void increment_bytes(uint64_t nbytes);

      /**
       * @brief Increment bytes_dropped by the specified number of packets.
       *
       * @param num_packets The number of packets that were dropped.
       */
      void dropped(uint64_t num_packets=1);

      /**
       * @brief Increment bytes_dropped by the specified amount.
       *
       * @param nbytes Number of bytes to incremenet bytes_dropped.
       */
      void dropped_bytes(uint64_t nbytes);

      /**
       * @brief Incremenet the number of valid packets that were ignored due to PSN/timestamps.
       * 
       * @param num_packets The number of valid packets that were ignored 
       */
      void ignored(uint64_t num_packets=1);

      /**
       * @brief Increment the number of busy-wait sleeps by the specified amount.
       *
       * @param num_sleeps Number of busy-wait sleeps
       */
      void sleeps(uint64_t num_sleeps=1);

      /**
       * @brief Increment the number of malformed packets
       * 
       * @param num_packets 
       */
      void malformed(uint64_t num_packets=1);

      /**
       * @brief Increment the number of misordered packets
       * 
       * @param num_packets 
       */
      void misordered(uint64_t num_packets=1);

      /**
       * @brief Increment the number of misdirected packets
       * 
       * @param num_packets 
       */
      void misdirected(uint64_t num_packets=1);

      /**
       * @brief Increment the number of discarded packets
       * 
       * @param num_packets 
       */
      void discarded(uint64_t num_packets=1);

      /**
       * @brief Reset the internal counters.
       *
       */
      void reset();

      /**
       * @brief Return the amount of data of data transmitted.
       *
       * @return uint64_t Number of bytes transmitted.
       */
      uint64_t get_data_transmitted() const { return bytes_transmitted; }

      /**
       * @brief Return the number of packets transmitted.
       *
       * @return uint64_t Number of packets transmitted
       */
      uint64_t get_packets_transmitted() const;

      /**
       * @brief Return the number of bytes dropped.
       *
       * @return uint64_t Number of bytes dropped.
       */
      uint64_t get_data_dropped() const { return bytes_dropped; }

      /**
       * @brief Return the number of packets dropped.
       *
       * @return uint64_t Number of packets dropped.
       */
      uint64_t get_packets_dropped() const;

      /**
       * @brief Get the number of discarded packets
       * 
       * @return uint64_t Number of discarded packets
       */
      uint64_t get_discarded() const { return discarded_packets; }

      /**
       * @brief Return the number of busy-wait sleeps.
       *
       * @return uint64_t Number of busy-wait sleeps.
       */
      uint64_t get_nsleeps() const { return nsleeps; }

      /**
       * @brief Return the number of valid packets that were ignored
       * 
       * @return uint64_t Number of ignored packets
       */
      uint64_t get_ignored() const { return ignored_packets; }

      /**
       * @brief Return the number of malformed packets
       * 
       * @return uint64_t Number of malformed packets
       */
      uint64_t get_malformed() const { return malformed_packets; }

      /**
       * @brief Get the number of misdirected packets
       * 
       * @return uint64_t Number of misdirected packets
       */
      uint64_t get_misdirected() const { return misdirected_packets; }

      /**
       * @brief Get the number of misordered packets
       * 
       * @return uint64_t Number of misordered packets
       */
      uint64_t get_misordered() const { return misordered_packets; }

      /**
       * @brief Compute the instantaneous data rates
       *
       */
      void compute_rates();

      /**
       * @brief Get the data transmission rate.
       *
       * @return float data transmission rate in bytes per second.
       */
      double get_data_transmission_rate() const;

      /**
       * @brief Get the data drop rate
       *
       * @return float data drop rate in bytes per second.
       */
      double get_data_drop_rate() const;

      /**
       * @brief Get the number of busy-wait sleeps per second
       *
       * @return double busy wait sleeps per second.
       */
      double get_sleep_rate() const;

      /**
       * @brief Increment the number of packet timeout
       * 
       */
      void packet_receive_timeout();

      /**
       * @brief Get the packet receive timeouts object
       * 
       * @return uint64_t returns packet_receive_timeouts
       */
      uint64_t get_packet_receive_timeouts() const;
      
      /**
       * @brief Set the packet timeout threshold object
       * 
       * @param threshold timeout threshold between packets
       */
      void set_packet_timeout_threshold(uint64_t threshold);
      
      /**
       * @brief Set the initial packet timeout threshold object
       * 
       * @param threshold Timeout threshold when waiting for the first packet
       */
      void set_initial_packet_timeout_threshold(uint64_t threshold);
      
      /**
       * @brief Get the packet timeout threshold object
       * 
       * @return uint64_t returns packet_timeout_threshold. Default value of 1
       */
      uint64_t get_packet_timeout_threshold();
      
      /**
       * @brief Get the initial packet timeout threshold object
       * 
       * @return uint64_t returns initial_packet_timeout_threshold. Default value of 10
       */
      uint64_t get_initial_packet_timeout_threshold();


    private:

      //! Size of data/samples in each packet
      unsigned packet_data_size{0};

      //! Total size of packet data, including data, weights and headers
      unsigned packet_size{0};

      //! Total number of bytes transmitted
      uint64_t bytes_transmitted{0};

      //! Total number of bytes dropped
      uint64_t bytes_dropped{0};

      //! Total number of busy-wait sleeps
      uint64_t nsleeps{0};

      //! Total number of ignored packets
      uint64_t ignored_packets{0};

      //! Total number of discarded packets
      uint64_t discarded_packets{0};

      //! Total number of malrformed packets
      uint64_t malformed_packets{0};

      //! Total number of misdirected packets
      uint64_t misordered_packets{0};

      //! Total number of misordered packets
      uint64_t misdirected_packets{0};

      //! Total number of packet receive timeouts in a scan
      uint64_t packet_receive_timeouts{0};

      //! Timeout threshold for the first packet
      uint64_t initial_packet_timeout_threshold = 10;

      //! Timeout threshold between packets
      uint64_t packet_timeout_threshold = 1;

      //! Mutex to prevent multiple threads from updating the timer
      std::mutex compute_rates_mutex;

      //! Timer that records the elapsed time between calculates of the data rate
      ska::pst::common::Timer timer{};

      //! Timer that records the elapsed time between packets received
      ska::pst::common::Timer timeout_timer{};

      //! Number of bytes transmitted when the timer was reset
      uint64_t prev_bytes_transmitted{0};

      //! Number of bytes dropped when the timer was reset
      uint64_t prev_bytes_dropped{0};

      //! Number of bytes dropped when the timer was reset
      uint64_t prev_nsleeps{0};

      //! Instantaneous data rate for bytes
      double bytes_transmitted_per_second{0};

      //! Instantaneous data rate most recently computed
      double bytes_dropped_per_second{0};

      //! Instantaneous number of busy-wait sleeps per second
      double nsleeps_per_second{0};

      //! Minimum elapsed time between sampling in seconds
      double minimum_elapsed{0.001};
  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_UDPStats_h
