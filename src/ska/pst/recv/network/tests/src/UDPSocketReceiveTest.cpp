/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <thread>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/network/tests/UDPSocketReceiveTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

static constexpr unsigned default_udp_port = 12345;
static constexpr unsigned default_bufsz = 2048;
static constexpr size_t default_packet_size = 1024;
static constexpr size_t default_buffer_size = 32768;

UDPSocketReceiveTest::UDPSocketReceiveTest()
    : ::testing::Test()
{
}

void UDPSocketReceiveTest::SetUp()
{
}

void UDPSocketReceiveTest::TearDown()
{
}

void UDPSocketReceiveTest::send_after_sleep(int delay_ms)
{
    int delay_us = delay_ms * ska::pst::recv::microseconds_per_millisecond;
    usleep(delay_us);
    sock_send.send();
}

TEST_F(UDPSocketReceiveTest, test_default_constructor) // NOLINT
{
  UDPSocketReceive sock; 
}

TEST_F(UDPSocketReceiveTest, test_resize) // NOLINT
{
  UDPSocketReceive sock;
  sock.resize(default_bufsz);
  ASSERT_EQ(sock.get_bufsz(), default_bufsz);
}

TEST_F(UDPSocketReceiveTest, test_configure) // NOLINT
{
  UDPSocketReceive sock;

  sock.allocate_resources(default_buffer_size, default_packet_size);
  sock.configure("127.0.0.1", default_udp_port);
  ASSERT_GE(sock.get_fd(), 0);
  sock.deconfigure_beam();

  sock.allocate_resources(default_buffer_size, default_packet_size);
  sock.configure("any", default_udp_port);
  ASSERT_GE(sock.get_fd(), 0);
  sock.deconfigure_beam();

  // bad port number
  sock.allocate_resources(default_buffer_size, default_packet_size);
  EXPECT_THROW(sock.configure("127.0.0.1", -1), std::runtime_error); // NOLINT

  // bad IP address
  EXPECT_THROW(sock.configure("257.0.0.1", default_udp_port), std::runtime_error); // NOLINT

  // IP address that is not bindale
  EXPECT_THROW(sock.configure("123.123.123.123", default_udp_port), std::runtime_error); // NOLINT
}

TEST_F(UDPSocketReceiveTest, test_release_packet) // NOLINT
{
  UDPSocketReceive sock;
  sock.release_packet(0);
}

TEST_F(UDPSocketReceiveTest, test_still_receiving) // NOLINT
{
  UDPSocketReceive sock;
  ASSERT_EQ(sock.still_receiving(), true);
}

TEST_F(UDPSocketReceiveTest, test_clear_buffered_packets) // NOLINT
{
  UDPSocketReceive sock_recv;

  sock_recv.allocate_resources(default_buffer_size, default_packet_size);
  sock_recv.configure("127.0.0.1", default_udp_port);

  sock_send.open("127.0.0.1", default_udp_port, "0.0.0.0");

  sock_recv.resize(default_packet_size);
  sock_send.resize(default_packet_size);

  sock_send.send(default_packet_size);
  ASSERT_EQ(sock_recv.clear_buffered_packets(), default_packet_size);

  sock_recv.set_block();

  sock_send.send(default_packet_size);
  ASSERT_EQ(sock_recv.clear_buffered_packets(), default_packet_size);

  sock_recv.deconfigure_beam();
  sock_send.close_me();
}

TEST_F(UDPSocketReceiveTest, test_acquire_packet) // NOLINT
{
  UDPSocketReceive sock_recv;

  sock_recv.allocate_resources(default_buffer_size, default_packet_size);
  sock_recv.configure("127.0.0.1", default_udp_port);
  sock_send.open("127.0.0.1", default_udp_port, "0.0.0.0");
  
  sock_recv.resize(default_packet_size);
  sock_send.resize(default_packet_size);

  sock_send.send(default_packet_size);

  int slot = sock_recv.acquire_packet();
  ASSERT_EQ(slot, 0);
  sock_recv.release_packet(slot);

  sock_recv.set_block();

  sock_send.send(default_packet_size);
  slot = sock_recv.acquire_packet();
  ASSERT_EQ(slot, 0);
  sock_recv.release_packet(slot);

  sock_recv.set_nonblock();

  static constexpr size_t too_small_packet_size = 8;
  sock_send.send(too_small_packet_size);
  slot = sock_recv.acquire_packet();
  ASSERT_EQ(slot, ska::pst::recv::MALFORMED_PACKET);

  sock_recv.deconfigure_beam();
  sock_send.close_me();
}

TEST_F(UDPSocketReceiveTest, test_recv_from_nonblocking) // NOLINT
{
  UDPSocketReceive sock_recv;
  sock_recv.allocate_resources(default_buffer_size, default_packet_size);
  sock_recv.configure("127.0.0.1", default_udp_port);
  sock_send.open("127.0.0.1", default_udp_port, "0.0.0.0");

  static constexpr int delay_ms = 100;
  std::thread delay_thread = std::thread(&UDPSocketReceiveTest::send_after_sleep, this, delay_ms);
  sock_recv.set_nonblock();
  sock_recv.acquire_packet();
  delay_thread.join();

  ASSERT_GE(sock_recv.process_sleeps(), 0);
  sock_recv.deconfigure_beam();
  sock_send.close_me();
}

} // namespace ska::pst::recv::test