/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/network/tests/PacketUtilsTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

PacketUtilsTest::PacketUtilsTest()
    : ::testing::Test()
{
}

void PacketUtilsTest::SetUp()
{
  static constexpr size_t packet_size = 128;
  raw.resize(packet_size);
  for (unsigned i=0; i<packet_size; i++)
  {
    raw[i] = static_cast<char>(i);
  }
  // raw[0] = 0x00;
  // raw[1] = 0x01;
  // raw[2] = 0x02;
  // raw[3] = 0x03;
  // raw[4] = 0x04;
  // raw[5] = 0x05;
  // raw[6] = 0x06;
  // raw[7] = 0x07;
  // raw[8] = 0x08;
  // raw[9] = 0x09;
  // raw[10] = 0x0A;
  // raw[11] = 0x0B;
  // raw[12] = 0x0C;
  // raw[13] = 0x0D;
  // raw[14] = 0x0E;
  // raw[15] = 0x0F;
  // raw[16] = 0x10;
  // raw[17] = 0x11;
  // raw[18] = 0x12;
  // raw[19] = 0x13;
}

void PacketUtilsTest::TearDown()
{
}

TEST_F(PacketUtilsTest, test_PacketBuffer_construct_destruct) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);

  {
    PacketBuffer pb;
    ASSERT_EQ(pb.data(), nullptr);
    ASSERT_EQ(pb.size(), 0);
  }

  {
    PacketBuffer pb(&buffer[0], bufsz);
    ASSERT_EQ(pb.data(), &buffer[0]);
    ASSERT_EQ(pb.size(), bufsz);
  }
}

TEST_F(PacketUtilsTest, test_EthernetFrame_construct_destruct) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);
  {
    EthernetFrame eth(&buffer[0], bufsz);
  }

  {
    static constexpr size_t too_small_size = EthernetFrame::min_size - 1;
    EXPECT_THROW(EthernetFrame eth(&buffer[0], too_small_size), std::length_error); // NOLINT
  }
}

TEST_F(PacketUtilsTest, test_IPv4Packet_construct_destruct) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);

  {
    // Ethernet Frame minimum size is 14 bytes
    EthernetFrame eth(&buffer[0], bufsz);
    ASSERT_EQ(eth.data(), &buffer[0]);
    ASSERT_EQ(eth.size(), bufsz);

    // IPv4 Packet minimu size is 20 bytes
    IPv4Packet ip = eth.payload_ipv4();
    size_t offset = EthernetFrame::min_size;
    ASSERT_EQ(ip.data(), &buffer[offset]);
    ASSERT_EQ(ip.size(), bufsz - offset);
  }

  {
    static constexpr size_t too_small_size = IPv4Packet::min_size - 1;
    EXPECT_THROW(IPv4Packet ip(&buffer[0], too_small_size), std::length_error); // NOLINT
  }
}

TEST_F(PacketUtilsTest, test_IPv4Packet_accessors) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);
  IPv4Packet ip(&buffer[0], bufsz);

  buffer[0] = 0x5A; // NOLINT 0101 1010

  // version is the upper 4 bits of byte 0,
  ASSERT_EQ(ip.version(), 5);

  // header length is 4 * lower 4 bits of byte 0
  ASSERT_EQ(ip.header_length(), 40);

  // encode 65281 as 0xFF01
  buffer[6] = 0xFF; // NOLINT
  buffer[7] = 0x01; // NOLINT
  static constexpr uint16_t flags_frag_off = 65281;
  ASSERT_EQ(ip.flags_frag_off(), flags_frag_off);

  uint16_t more_fragments = (IPv4Packet::flag_more_fragments | IPv4Packet::flag_more_fragments_bitmask);
  ASSERT_EQ(ip.is_fragment(), bool(ip.flags_frag_off() & more_fragments));
}

TEST_F(PacketUtilsTest, test_UDPPacket) // NOLINT
{
  UDPPacket udp(&raw[0], raw.size());

  // converted BE hex values from the raw vector (see constructor) into their expected values
  static constexpr uint16_t source_port = 1; // NOLINT 0x0001
  static constexpr uint16_t destination_port = 515; // NOLINT  0x0203
  static constexpr uint16_t length = 1029; // NOLINT  0x0405
  static constexpr uint16_t checksum = 1543; // NOLINT  0x0607

  // assert that the decoded values are indeed correct
  ASSERT_EQ(udp.source_port(), source_port);
  ASSERT_EQ(udp.destination_port(), destination_port);
  ASSERT_EQ(udp.length(), length);
  ASSERT_EQ(udp.checksum(), checksum);
}

TEST_F(PacketUtilsTest, test_UDPPacket_from_IPv4Packet) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);

  buffer[0] = 0x45; // NOLINT IPv4 and header length of 20
  buffer[2] = 0x00; // NOLINT total length of 44
  buffer[3] = 0x2C; // NOLINT total length of 44

  {
    IPv4Packet ip(&buffer[0], buffer.size());
    UDPPacket udp = ip.payload_udp(); // NOLINT
  }

  {
    buffer[2] = 0x00; // NOLINT total length of 28
    buffer[3] = 0x1C; // NOLINT total length of 28
    IPv4Packet ip(&buffer[0], IPv4Packet::min_size + UDPPacket::min_size);
    UDPPacket udp = ip.payload_udp();  // NOLINT
  }

  {
    buffer[2] = 0x00; // NOLINT total length of 27
    buffer[3] = 0x1B; // NOLINT total length of 27
    IPv4Packet ip(&buffer[0], IPv4Packet::min_size + UDPPacket::min_size);
    EXPECT_THROW(UDPPacket udp = ip.payload_udp(), std::length_error); // NOLINT
  }
}

TEST_F(PacketUtilsTest, test_PacketBuffer_from_UDPPacket) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);

  buffer[4] = 0x00; // NOLINT length of 64
  buffer[5] = 0x40; // NOLINT length of 64
  {
    UDPPacket udp(&buffer[0], buffer.size());
    PacketBuffer pb = udp.payload(); // NOLINT
    ASSERT_EQ(pb.size(), buffer.size() - UDPPacket::min_size);
  }

  buffer[4] = 0x00; // NOLINT length of 7
  buffer[5] = 0x07; // NOLINT length of 7
  {
    UDPPacket udp(&buffer[0], buffer.size());
    EXPECT_THROW(PacketBuffer pb = udp.payload(), std::length_error); // NOLINT
  }
}

TEST_F(PacketUtilsTest, test_IPv4Packet) // NOLINT
{
  static constexpr uint8_t version_ihl = 0;        // NOLINT 0x00
  static constexpr uint8_t dscp_ecn = 1;           // NOLINT 0x01
  static constexpr uint16_t total_length = 515;    // NOLINT  x0203
  static constexpr uint16_t identification = 1029; // NOLINT 0x0405
  static constexpr uint16_t flags_frag_off = 1543; // NOLINT 0x0607
  static constexpr uint8_t ttl = 8;                // NOLINT 0x08
  static constexpr uint8_t protocol = 9;           // NOLINT 0x09
  static constexpr uint16_t checksum = 2826;       // NOLINT 0x0B0A
  static constexpr ip_addr_v4 source_address = {12, 13, 14, 15};
  static constexpr ip_addr_v4 destination_address = {16, 17, 18, 19};

  IPv4Packet ip(&raw[0], raw.size());

  ASSERT_EQ(ip.version_ihl(), version_ihl);
  ASSERT_EQ(ip.dscp_ecn(), dscp_ecn);
  ASSERT_EQ(ip.total_length(), total_length);
  ASSERT_EQ(ip.identification(), identification);
  ASSERT_EQ(ip.flags_frag_off(), flags_frag_off);
  ASSERT_EQ(ip.ttl(), ttl);
  ASSERT_EQ(ip.protocol(), protocol);
  ASSERT_EQ(ip.checksum(), checksum);
  ASSERT_EQ(ip.source_address(), source_address);
  ASSERT_EQ(ip.destination_address(), destination_address);
}

TEST_F(PacketUtilsTest, test_EthernetFrame) // NOLINT
{
  EthernetFrame eth(&raw[0], raw.size());

  static constexpr mac_address source_mac = { 6, 7, 8, 9, 10, 11 };
  static constexpr mac_address destination_mac = { 0, 1, 2, 3, 4, 5 };
  static constexpr uint16_t ethertype = 3085; // NOLINT 0x0C0D
  ASSERT_EQ(eth.destination_mac(), destination_mac);
  ASSERT_EQ(eth.source_mac(), source_mac);
  ASSERT_EQ(eth.ethertype(), ethertype);
}

TEST_F(PacketUtilsTest, test_udp_from_ethernet) // NOLINT
{
  static constexpr size_t bufsz = 64;
  std::vector<char> buffer(bufsz);

  // bytes 0-13 Ethernet header
  // bytes 14-34 IPv4 header
  // bytes 34-42 UDP header
  // bytes 42-64 UDP data

  // Ethernet
  buffer[12] = 0x08; // NOLINT ethertype for IP
  buffer[13] = 0x00; // NOLINT ethertype for IP

  // IPv4
  buffer[14] = 0x45; // NOLINT IPv4 and header length of 20
  buffer[16] = 0x00; // NOLINT total length of 44
  buffer[17] = 0x2C; // NOLINT total length of 44
  buffer[23] = ska::pst::recv::UDPPacket::protocol; // NOLINT UDP protocol packet

  // UDP
  buffer[38] = 0x00; // NOLINT length of 22
  buffer[39] = 0x16; // NOLINT length of 22


  EthernetFrame eth(&buffer[0], bufsz);
  // should be correct, happy path
  {
    SPDLOG_DEBUG("PacketUtilsTest::test_udp_from_ethernet testing correct configuration");
    PacketBuffer pb = ska::pst::recv::udp_from_ethernet(eth.data(), eth.size()); // NOLINT
  }

  // test good/bad ethertype
  {
    buffer[12] = 0x00; // NOLINT
    SPDLOG_DEBUG("PacketUtilsTest::test_udp_from_ethernet testing bad ethertype");
    EXPECT_THROW(ska::pst::recv::udp_from_ethernet(eth.data(), eth.size()), std::runtime_error); // NOLINT
    buffer[12] = 0x08; // NOLINT
  }

  // test bad/good IPv4 version
  {
    // version is the upper 4 bits of byte 0,
    buffer[14] = 0x05; // NOLINT
    SPDLOG_DEBUG("PacketUtilsTest::test_udp_from_ethernet testing bad IPv4 version");
    EXPECT_THROW(ska::pst::recv::udp_from_ethernet(eth.data(), eth.size()), std::runtime_error); // NOLINT
    buffer[14] = 0x45; // NOLINT
  }

  // test UDP_protocol
  {
    buffer[23] = 0x00; // NOLINT
    SPDLOG_DEBUG("PacketUtilsTest::test_udp_from_ethernet testing fragmented packet");
    EXPECT_THROW(ska::pst::recv::udp_from_ethernet(eth.data(), eth.size()), std::runtime_error); // NOLINT
    buffer[23] = ska::pst::recv::UDPPacket::protocol; // NOLINT
  }

  // test fragmented packet
  {
    buffer[20] = 0xFF; // NOLINT
    SPDLOG_DEBUG("PacketUtilsTest::test_udp_from_ethernet testing fragmented packet");
    EXPECT_THROW(ska::pst::recv::udp_from_ethernet(eth.data(), eth.size()), std::runtime_error); // NOLINT
    buffer[20] = 0x00; // NOLINT
  }
}

TEST_F(PacketUtilsTest, test_interface_mac) // NOLINT
{
  // todo consider if such a test is reasonable in docker
  // mac_address addr = interface_mac("172.21.0.2");
}

} // namespace ska::pst::recv::test
