/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <memory>
#include <vector>

#ifndef SKA_PST_RECV_NETWORK_Socket_h
#define SKA_PST_RECV_NETWORK_Socket_h

namespace ska::pst::recv {

  /**
   * @brief Abstract base class for a UDP, TCP or IBV Socket
   * 
   */
  class Socket {

    public:

      /**
       * @brief Construct a new Socket object
       * 
       */
      Socket();

      /**
       * @brief Destroy the Socket object
       * 
       */
      ~Socket();

      /**
       * @brief Opens the socket on the specified port and set the file descriptor.
       * 
       * @param port 
       */
      virtual void open(int port) = 0;

      /**
       * @brief Close the socket file descriptor.
       * 
       */
      void close_me();

      /**
       * @brief Resize the socket buffer to the specified size.
       * 
       * @param new_bufsz new socket size in bytes
       */
      virtual void resize(size_t new_bufsz);

      /**
       * @brief Set the socket buffer to zero.
       * 
       */
      void reset_buffer();

      /**
       * @brief Configure the socket as non blocking.
       * 
       * @return int 
       */
      int set_nonblock();

      /**
       * @brief Configure the socket as blocking.
       * 
       * @return int 
       */
      int set_block();

      /**
       * @brief Get the blocking object
       * 
       * @return char 
       */
      char get_blocking() { return ! non_block; };

      /**
       * @brief Return the socket file descriptor
       * 
       * @return int internal file descriptor
       */
      int get_fd() { return fd; };

      /**
       * @brief Return a pointer to the socket buffer
       * 
       * @return char* 
       */
      char * get_buf() { return &buf[0]; };

      /**
       * @brief Return the size of the socket buffer.
       * 
       * @return size_t socket buffer size in bytes.
       */
      size_t get_bufsz() { return buf.size(); };

    protected:

      /**
       * @brief Socket file descriptor.
       * 
       */
      int fd{0};

      /**
       * @brief Socket buffer
       * 
       */
      std::vector<char> buf;

      //! Socket port number
      int port{0};

    private:

      //! IPv4 address of socket's interface
      std::string interface;

      //! flag for blocking I/O state on socket
      char non_block{0};

  };

} // namespace ska::pst::recv

#endif // SKA_PST_RECV_NETWORK_Socket_h
