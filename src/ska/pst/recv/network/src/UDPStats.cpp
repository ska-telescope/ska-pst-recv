/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/network/UDPStats.h"


ska::pst::recv::UDPStats::UDPStats()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPStats::UDPStats");
}

void ska::pst::recv::UDPStats::configure(UDPFormat * format)
{
  SPDLOG_DEBUG("ska::pst::recv::UDPStats::configure");
  packet_data_size = format->get_packet_data_size();
  packet_size = format->get_packet_size();
  reset();
}

void ska::pst::recv::UDPStats::reset()
{
  SPDLOG_DEBUG("ska::pst::recv::UDPStats::reset");
  bytes_transmitted = 0;
  bytes_dropped = 0;
  nsleeps = 0;
  timer.reset();
  timeout_timer.reset();
  prev_bytes_transmitted = 0;
  prev_bytes_dropped = 0;
  prev_nsleeps = 0;
  bytes_transmitted_per_second = 0;
  bytes_dropped_per_second = 0;
  nsleeps_per_second = 0;
  ignored_packets = 0;
  discarded_packets = 0;
  malformed_packets = 0;
  misdirected_packets = 0;
  misordered_packets = 0;
  packet_receive_timeouts = 0;
}

void ska::pst::recv::UDPStats::increment(uint64_t num_packets)
{
  increment_bytes(num_packets * packet_size);
}

void ska::pst::recv::UDPStats::increment_bytes(uint64_t nbytes)
{
  bytes_transmitted += nbytes;
}

void ska::pst::recv::UDPStats::dropped_bytes(uint64_t nbytes)
{
  bytes_dropped += nbytes;
}

void ska::pst::recv::UDPStats::dropped(uint64_t ndropped)
{
  dropped_bytes(ndropped * packet_size);
}

void ska::pst::recv::UDPStats::sleeps(uint64_t to_add)
{
  nsleeps += to_add;
}

void ska::pst::recv::UDPStats::ignored(uint64_t to_add)
{
  ignored_packets += to_add;
}

void ska::pst::recv::UDPStats::malformed(uint64_t to_add)
{
  malformed_packets  += to_add;
}

void ska::pst::recv::UDPStats::misdirected(uint64_t to_add)
{
  misdirected_packets  += to_add;
}

void ska::pst::recv::UDPStats::misordered(uint64_t to_add)
{
  misordered_packets  += to_add;
}

void ska::pst::recv::UDPStats::discarded(uint64_t to_add)
{
  discarded_packets  += to_add;
}

void ska::pst::recv::UDPStats::packet_receive_timeout()
{
  packet_receive_timeouts++;
}

auto ska::pst::recv::UDPStats::get_packet_receive_timeouts() const -> uint64_t
{
  return packet_receive_timeouts;
}

auto ska::pst::recv::UDPStats::get_packets_transmitted() const -> uint64_t
{
  if (packet_data_size == 0)
  {
    throw std::runtime_error("ska::pst::recv::UDPStats::get_packets_transmitted packet_data_size not configured");
  }
  return bytes_transmitted / packet_data_size;
}

auto ska::pst::recv::UDPStats::get_packets_dropped() const -> uint64_t
{
  if (packet_data_size == 0)
  {
    throw std::runtime_error("ska::pst::recv::UDPStats::get_packets_dropped packet_data_size not configured");
  }
  return bytes_dropped / packet_data_size;
}

void ska::pst::recv::UDPStats::set_packet_timeout_threshold(uint64_t threshold)
{
  SPDLOG_DEBUG("set_packet_timeout_threshold() threshold={}", threshold);
  packet_timeout_threshold = threshold;
}

void ska::pst::recv::UDPStats::set_initial_packet_timeout_threshold(uint64_t threshold)
{
  SPDLOG_DEBUG("set_initial_packet_timeout_threshold() threshold={}", threshold);
  initial_packet_timeout_threshold = threshold;
}

auto ska::pst::recv::UDPStats::get_packet_timeout_threshold() -> uint64_t
{
  SPDLOG_DEBUG("get_packet_timeout_threshold()");
  return packet_timeout_threshold;
}

auto ska::pst::recv::UDPStats::get_initial_packet_timeout_threshold() -> uint64_t
{
  SPDLOG_DEBUG("get_initial_packet_timeout_threshold()");
  return initial_packet_timeout_threshold;
}

void ska::pst::recv::UDPStats::compute_rates()
{
  compute_rates_mutex.lock();
  double elapsed_seconds = timer.get_elapsed_microseconds() * seconds_per_microseconds;
  auto transmitted = static_cast<double>(bytes_transmitted - prev_bytes_transmitted);
  auto dropped = static_cast<double>(bytes_dropped - prev_bytes_dropped);
  auto sleeps = static_cast<double>(nsleeps - prev_nsleeps);

  // only compute real rates if a minimum sampling time has elapsed
  if (elapsed_seconds > minimum_elapsed)
  {
    bytes_transmitted_per_second = transmitted / elapsed_seconds;
    bytes_dropped_per_second = dropped / elapsed_seconds;
    nsleeps_per_second = sleeps / elapsed_seconds;

    if(transmitted > 0)
    {
      timeout_timer.reset();
    }
    else
    {
      double elapsed_timout_seconds = timeout_timer.get_elapsed_microseconds() * seconds_per_microseconds;
      auto timeout_threshold = static_cast<double>(packet_timeout_threshold);
      if(bytes_transmitted == 0)
      {
        timeout_threshold = static_cast<double>(initial_packet_timeout_threshold);
      }
      if(elapsed_timout_seconds > timeout_threshold)
      {
        packet_receive_timeout();
      }
    }

    // reset the timer
    timer.reset();
  }
  else
  {
    bytes_transmitted_per_second = 0;
    bytes_dropped_per_second = 0;
    nsleeps_per_second = 0;
  }

  prev_bytes_transmitted = bytes_transmitted;
  prev_bytes_dropped = bytes_dropped;
  compute_rates_mutex.unlock();
}

auto ska::pst::recv::UDPStats::get_data_transmission_rate() const -> double
{
  return bytes_transmitted_per_second;
}

auto ska::pst::recv::UDPStats::get_data_drop_rate() const -> double
{
  return bytes_dropped_per_second;
}

auto ska::pst::recv::UDPStats::get_sleep_rate() const -> double
{
  return nsleeps_per_second;
}
