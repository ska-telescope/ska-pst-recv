/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>
#include <utility>

#include "ska/pst/recv/formats/MidPST.h"

static constexpr unsigned UDP_FORMAT_CBFPSR_PACKET_NCHAN = 185;
static constexpr unsigned UDP_FORMAT_CBFPSR_PACKET_NSAMP = 4;
static constexpr unsigned UDP_FORMAT_CBFPSR_NSAMP_PER_WEIGHT  = 4;
static constexpr unsigned UDP_FORMAT_CBFPSR_OS_NUMERATOR = 8;
static constexpr unsigned UDP_FORMAT_CBFPSR_OS_DENOMINATOR = 7;

ska::pst::recv::MidPST::MidPST()
{
  nsamp_per_packet = UDP_FORMAT_CBFPSR_PACKET_NSAMP;
  nchan_per_packet = UDP_FORMAT_CBFPSR_PACKET_NCHAN;
  nsamp_per_weight = UDP_FORMAT_CBFPSR_NSAMP_PER_WEIGHT;
  destination = UDP_FORMAT_CBFPSR_MID_PST;
}

auto ska::pst::recv::MidPST::get_samples_per_packet() -> unsigned
{
  return UDP_FORMAT_CBFPSR_PACKET_NSAMP;
}

auto ska::pst::recv::MidPST::get_expected_os_factor() -> std::pair<uint32_t, uint32_t>
{
  return std::make_pair(UDP_FORMAT_CBFPSR_OS_NUMERATOR, UDP_FORMAT_CBFPSR_OS_DENOMINATOR);
}
