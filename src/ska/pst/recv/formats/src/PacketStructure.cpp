/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>
#include <spdlog/spdlog.h>

#include "ska/pst/recv/formats/PacketStructure.h"

/**
 * @brief Configure default parameters for the UDP packets
 * 
 * @param header 
 */
void ska::pst::recv::configure_packet_defaults(cbf_psr_header_t * header)
{
  memset(header, 0, sizeof(cbf_psr_header_t));
  header->cbf_version_major = 0;
  header->cbf_version_minor = 1;
  header->packet_sequence_number = 1;
  header->first_channel_number = 0;
  header->timestamp_attoseconds = 0;
  header->timestamp_seconds = 0;
  header->os_ratio_numerator = 1;
  header->os_ratio_denominator = 1;
  header->beam_number = 1;
  header->scan_id = 1;
  header->magic_word = ska::pst::recv::magic_word;
}

auto ska::pst::recv::validate_packet_header(cbf_psr_header_t * header) -> bool
{
  return (header->magic_word == ska::pst::recv::magic_word);
}

auto ska::pst::recv::get_scale1_byte_offset() -> size_t
{
  cbf_psr_header_t header;
  return uintptr_t(&(header.scale_1)) - uintptr_t(&(header)); // NOLINT
};

void ska::pst::recv::print_packet_header(cbf_psr_header_t * header)
{
  SPDLOG_INFO("CBF version: {}.{}", uint32_t(header->cbf_version_major), uint32_t(header->cbf_version_minor));
  SPDLOG_INFO("Packet Sequence Number: {}", uint64_t(header->packet_sequence_number));
  SPDLOG_INFO("First Channel Number: {}", uint32_t(header->first_channel_number));
  SPDLOG_INFO("First Channel Freq: {} millihertz", uint64_t(header->first_channel_freq));
  SPDLOG_INFO("Timestamp: seconds={} attoseconds={}", uint32_t(header->timestamp_seconds), uint64_t(header->timestamp_attoseconds));
  SPDLOG_INFO("Channels per packet: number={} valid={}", uint32_t(header->channels_per_packet), uint64_t(header->valid_channels_per_packet));
  SPDLOG_INFO("Time samples: per packet={} per relative weight={}", uint32_t(header->time_samples_per_packet), uint32_t(header->num_time_samples_per_relative_weight));
  SPDLOG_INFO("Beam Number: {}", uint32_t(header->beam_number));
  SPDLOG_INFO("Packet Destination: {}", uint32_t(header->packet_destination));
  SPDLOG_INFO("Data Precision: {} bits/value", uint32_t(header->data_precision));
  SPDLOG_INFO("Oversampling Ratio: {}/{}", uint32_t(header->os_ratio_numerator), uint32_t(header->os_ratio_denominator));
  SPDLOG_INFO("Scan ID: {}", uint32_t(header->scan_id));
  SPDLOG_INFO("Scales: {} {} {} {}", float(header->scale_1), float(header->scale_2), float(header->scale_3), float(header->scale_4));
  SPDLOG_INFO("Offsets: {} {} {} {}", float(header->offset_1), float(header->offset_2), float(header->offset_3), float(header->offset_4));
}