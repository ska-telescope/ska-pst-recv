/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/recv/formats/tests/UDPFormatTest.h"
#include "ska/pst/recv/formats/UDPHeader.h"
#include "ska/pst/recv/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv);
}

namespace ska::pst::recv::test {

UDPFormatTest::UDPFormatTest()
    : ::testing::Test()
{
}

void UDPFormatTest::SetUp()
{
  config.load_from_file(test_data_file("UDPFormatConfig.txt"));
  header.load_from_file(test_data_file("UDPFormatHeader.txt"));
}

void UDPFormatTest::TearDown()
{
  config.reset();
  header.reset();
}

auto UDPFormatTest::compute_data_resolution(unsigned nchan, unsigned npol, unsigned ndim, unsigned nbit, const ConcreteUDPFormat& format) -> unsigned
{
  unsigned resolution = nchan * npol * ndim * nbit * format.get_samples_per_packet() / bits_per_byte;
  return resolution;
}

auto UDPFormatTest::compute_weights_resolution(unsigned nchan, const ConcreteUDPFormat& format) -> unsigned
{
  static constexpr unsigned ndim = 1;
  static constexpr unsigned npol = 1;
  static constexpr unsigned nbit = UDP_FORMAT_CBFPSR_WEIGHT_NBIT;
  unsigned scale_factor_resolution = (nchan / format.get_nchan_per_packet()) * sizeof(float);
  unsigned weights_data_resolution = (nchan * npol * ndim * nbit * format.get_samples_per_packet()) / format.get_nsamp_per_weight() / bits_per_byte;
  return scale_factor_resolution + weights_data_resolution;
}

TEST_F(UDPFormatTest, test_default_constructor) // NOLINT
{
  ConcreteUDPFormat format;
}

TEST_F(UDPFormatTest, test_configure) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
}

TEST_F(UDPFormatTest, test_configure_with_bad_config)  // NOLINT
{
  ConcreteUDPFormat format;

  unsigned nsamp_per_weight = format.get_nsamp_per_weight();
  unsigned nchan_per_packet = format.get_nchan_per_packet();
  unsigned nsamp_per_packet = format.get_nsamp_per_packet();

  format.set_nsamp_per_weight(0);
  EXPECT_THROW(format.configure_beam(config), std::runtime_error); // NOLINT
  format.set_nsamp_per_weight(nsamp_per_weight);

  format.set_nchan_per_packet(0);
  EXPECT_THROW(format.configure_beam(config), std::runtime_error); // NOLINT
  format.set_nchan_per_packet(nchan_per_packet);

  format.set_nsamp_per_packet(0);
  EXPECT_THROW(format.configure_beam(config), std::runtime_error); // NOLINT
  format.set_nsamp_per_packet(nsamp_per_packet);

  static constexpr unsigned bad_nchan_per_packet = 123453453;
  format.set_nchan_per_packet(bad_nchan_per_packet);
  EXPECT_THROW(format.configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
  format.set_nchan_per_packet(nchan_per_packet);

  format.reset();

  ska::pst::common::AsciiHeader bad_config;
  bad_config.clone(config);

  // test that beam-ids between 1-16 are only valid
  static constexpr uint32_t too_low_beam_id = 0;
  static constexpr uint32_t too_high_beam_id = 17;
  bad_config.set("BEAM_ID", too_low_beam_id);
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT
  bad_config.set("BEAM_ID", too_high_beam_id);
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT
  for (unsigned beam_id=UDP_FORMAT_MIN_BEAM; beam_id<=UDP_FORMAT_MAX_BEAM; beam_id++)
  {
    bad_config.set("BEAM_ID", beam_id);
    EXPECT_NO_THROW(format.configure_beam(bad_config)); // NOLINT
    format.reset();
  }

  bad_config.clone(config);
  bad_config.set_val("NPOL", "3");
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT

  bad_config.clone(config);
  bad_config.set_val("NDIM", "3");
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT

  // over-ride nchan from 1024 (in config) to 64
  bad_config.clone(config);
  bad_config.set_val("NCHAN", "64");
  EXPECT_THROW(format.configure_beam(bad_config), ska::pst::common::pst_validation_error); // NOLINT
}

TEST_F(UDPFormatTest, test_prepare) // NOLINT
{
  ConcreteUDPFormat format;
  EXPECT_THROW(format.configure_scan(header), std::runtime_error); // NOLINT

  format.configure_beam(config);
  format.configure_scan(header);
}

TEST_F(UDPFormatTest, test_get_packet_data_size) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  ASSERT_EQ(format.get_packet_data_size(), 8192);
}

TEST_F(UDPFormatTest, test_get_packet_size) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  ASSERT_EQ(format.get_packet_size(), 8352);
}

TEST_F(UDPFormatTest, test_get_samples_per_packet) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  ASSERT_EQ(format.get_samples_per_packet(), 32);
}

TEST_F(UDPFormatTest, test_get_resolution) // NOLINT
{
  ConcreteUDPFormat format;
  EXPECT_THROW(format.get_resolution(), std::runtime_error); // NOLINT
  format.configure_beam(config);

  // compute the expected resolution
  unsigned nchan = config.get_uint32("NCHAN");
  unsigned npol = config.get_uint32("NPOL");
  unsigned ndim = config.get_uint32("NDIM");
  unsigned nbit = config.get_uint32("NBIT");
  unsigned resolution = compute_data_resolution(nchan, npol, ndim, nbit, format);
  ASSERT_EQ(format.get_resolution(), resolution);
}

TEST_F(UDPFormatTest, test_get_weights_resolution) // NOLINT
{
  ConcreteUDPFormat format;
  EXPECT_THROW(format.get_weights_resolution(), std::runtime_error); // NOLINT
  format.configure_beam(config);

  unsigned nchan = config.get_uint32("NCHAN");
  unsigned resolution = compute_weights_resolution(nchan, format);

  ASSERT_EQ(format.get_weights_resolution(), resolution);
}

TEST_F(UDPFormatTest, test_get_samples_for_bytes) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  unsigned nchan = config.get_uint32("NCHAN");
  unsigned npol = config.get_uint32("NPOL");
  unsigned ndim = config.get_uint32("NDIM");
  unsigned nbit = config.get_uint32("NBIT");
  unsigned resolution = compute_data_resolution(nchan, npol, ndim, nbit, format);

  ASSERT_EQ(format.get_samples_for_bytes(resolution-1), format.get_nsamp_per_packet()-1);
  ASSERT_EQ(format.get_samples_for_bytes(resolution), format.get_nsamp_per_packet());
  ASSERT_EQ(format.get_samples_for_bytes(resolution+1), format.get_nsamp_per_packet());
}

TEST_F(UDPFormatTest, test_decode_packet) // NOLINT
{
  ConcreteUDPFormat format;
  UDPHeader udp_header;

  // local header with which to test
  cbf_psr_header_t psr_header;
  configure_packet_defaults(&psr_header);
  char * header_ptr = reinterpret_cast<char *>(&psr_header);

  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  // configure UDP formats
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet configure_beam");
  format.configure_beam(config);
  udp_header.configure(config, format);

  // expect thrown exception as format has not been prepared
  EXPECT_THROW(format.decode_packet(header_ptr, &offsets, &sizes), std::runtime_error); // NOLINT

  // prepare the UDP format
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet configure_scan");
  format.configure_scan(header);

  static constexpr uint64_t scan_id = 1234;
  format.set_scan_id(scan_id);
  udp_header.set_scan_id(scan_id);
  udp_header.set_beam_id(config.get_uint32("BEAM_ID"));

  // write the header to the header_ptr
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet encode_header");
  udp_header.encode_header(header_ptr);

  // process the start of the data stream, configuring the start_psn
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet process_stream_start");
  format.process_stream_start(header_ptr);

  // check that malformed packets are correctly reported
  psr_header.magic_word = 0x0;
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet decode_packet malformed");
  ASSERT_EQ(format.decode_packet(header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Malformed);

  // configure sensible defaults for the packet
  psr_header.magic_word = ska::pst::recv::magic_word;

  // write UDP packet header to the header_ptr, then incremenent counters
  uint64_t packet_idx = 0;

  ASSERT_EQ(format.decode_packet(header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);
  ASSERT_EQ(offsets.data, packet_idx * format.get_packet_data_size());
  ASSERT_EQ(offsets.weights, packet_idx * (format.get_packet_weights_size() + format.get_packet_scales_size()));

  // print packet header to stdout
  udp_header.gen_packet(header_ptr);

  // increment format by 1 packet (32 channels in this case)
  packet_idx++;
  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet psr_header.first_channel_number={}", uint32_t(psr_header.first_channel_number));
  udp_header.gen_packet(header_ptr);

  SPDLOG_DEBUG("ska::pst::recv::UDPReceiver::UDPFormatTest::test_decode_packet psr_header.first_channel_number={}", uint32_t(psr_header.first_channel_number));
  ASSERT_EQ(format.decode_packet(header_ptr, &offsets, &sizes), ska::pst::recv::UDPFormat::PacketState::Ok);
  ASSERT_EQ(offsets.data, packet_idx * format.get_packet_data_size());
  ASSERT_EQ(offsets.weights, packet_idx * (format.get_packet_weights_size() + format.get_packet_scales_size()));

  // increment format by 1 packet (32 channels in this case)
  udp_header.gen_packet(header_ptr);
  packet_idx++;

  format.decode_packet(header_ptr, &offsets, &sizes);
  ASSERT_EQ(offsets.data, packet_idx * format.get_packet_data_size());
  ASSERT_EQ(offsets.weights, packet_idx * (format.get_packet_weights_size() + format.get_packet_scales_size()));
}

TEST_F(UDPFormatTest, test_insert_last_packet_and_weights) // NOLINT
{
  ConcreteUDPFormat format;
  format.configure_beam(config);
  format.configure_scan(header);
  UDPHeader udp_header;
  udp_header.configure(config, format);

  std::vector<char> packet(format.get_packet_size());
  std::vector<char> data_buffer(format.get_packet_data_size());
  std::vector<char> weights_buffer(format.get_packet_weights_size());
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  char * header_ptr = reinterpret_cast<char *>(&packet[0]);
  udp_header.encode_header(header_ptr);

  // process the start of the data stream, configuring the start_psn
  format.process_stream_start(header_ptr);
  format.decode_packet(header_ptr, &offsets, &sizes);
  format.insert_last_packet(&data_buffer[0], &weights_buffer[0]);
}

TEST_F(UDPFormatTest, test_gen_packet) // NOLINT
{
  ConcreteUDPFormat format;
  UDPHeader udp_header;
  format.configure_beam(config);
  format.configure_scan(header);

  udp_header.configure(config, format);

  static constexpr uint64_t scan_id = 12345;
  format.set_scan_id(scan_id);
  udp_header.set_scan_id(scan_id);

  // allocate storage for the packet
  std::vector<char> packet_data(format.get_packet_size());
  auto psr_header = reinterpret_cast<cbf_psr_header_t *>(&packet_data[0]);
  configure_packet_defaults(psr_header);
  auto psr_header_ptr = reinterpret_cast<char *>(psr_header);
  ska::pst::recv::UDPFormat::data_and_weights_t offsets{}, sizes{};

  // recorder the starting timestamp
  uint32_t timestamp_seconds = psr_header->timestamp_seconds;

  uint32_t nchan = config.get_uint32("NCHAN");
  uint32_t nchan_per_packet = format.get_nchan_per_packet();
  uint32_t npackets = nchan / nchan_per_packet;

  uint32_t psn = 1;

  // process the start of the data stream, configuring the start_psn
  format.process_stream_start(psr_header_ptr);

  while (psr_header->timestamp_seconds == timestamp_seconds)
  {
    // generate 1 full set for channels in the packet sequence
    for (unsigned i=0; i<npackets; i++)
    {
      udp_header.gen_packet(psr_header_ptr);
      format.decode_packet(psr_header_ptr, &offsets, &sizes);
      uint32_t pkt_idx = ((psn - 1) * npackets) + i;
      SPDLOG_TRACE("ska::pst::recv::test::UDPFormatTest::test_gen_packet pkt_idx={} offsets=({},{}) sizes=({},{})",
        pkt_idx, offsets.data, offsets.weights, sizes.data, sizes.weights);
      ASSERT_EQ(pkt_idx * sizes.data, offsets.data);
      ASSERT_EQ(pkt_idx * sizes.weights, offsets.weights);
      ASSERT_EQ(sizes.data, format.get_packet_data_size());
      ASSERT_EQ(sizes.weights, (format.get_packet_weights_size() + format.get_packet_scales_size()));
      ASSERT_EQ(psr_header->packet_sequence_number, psn);
    }
    psn++;
  }
}

} // namespace ska::pst::recv::test