/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstring>
#include <utility>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Time.h"
#include "ska/pst/common/utils/PacketLayout.h"
#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/recv/formats/PacketStructure.h"

#ifndef SKA_PST_RECV_FORMATS_UDPFormat_h
#define SKA_PST_RECV_FORMATS_UDPFormat_h

#define UDP_FORMAT_MIN_BEAM 1
#define UDP_FORMAT_MAX_BEAM 16
#define UDP_FORMAT_MAX_NBEAM 16
#define UDP_FORMAT_CBFPSR_NDIM 2
#define UDP_FORMAT_CBFPSR_NPOL 2
#define UDP_FORMAT_CBFPSR_WEIGHT_NBIT 16

#define UDP_FORMAT_CBFPSR_LOW_PSS 0
#define UDP_FORMAT_CBFPSR_MID_PSS 1
#define UDP_FORMAT_CBFPSR_LOW_PST 2
#define UDP_FORMAT_CBFPSR_MID_PST 3

namespace ska::pst::recv {

  /**
   * @brief Abstract base class for UDPFormats of the CBF/PSR data format
   *
   */
  class UDPFormat : public common::PacketLayout
  {
    public:

      /**
       * @brief Enumeration of the different types of received packet state
       * Ok            Packet received in good order
       * Malformed     Packet header is invalid and does not conform to the ICD definition
       * Misdirected   Packet header is valid but belongs to a different beam or sub-band
       * Misordered    Packet sequence number is wildly out of order.
       * Ignored       Packet is valid but preceeds the start_psn, so it is simply ignored whilst waiting for the start of the stream
       */
      enum PacketState {
        Ok,
        Malformed,
        Misdirected,
        Misordered,
        Ignored,
      };

      /**
       * @brief Convenience structure for data and weights used for offsets and sizes
       *
       */
      typedef struct data_and_weights
      {
        int64_t data;

        int64_t weights;

      } data_and_weights_t;

      /**
       * @brief Construct a new UDPFormat object
       *
       */
      UDPFormat();

      /**
       * @brief Destroy the UDPFormat object
       *
       */
      ~UDPFormat() = default;

      void validate_configure_beam(const ska::pst::common::AsciiHeader& beam_config, ska::pst::common::ValidationContext *context);
      void validate_configure_scan(const ska::pst::common::AsciiHeader& scan_config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Configure the UDPFormat from the fixed configuration
       *
       * @param beam_config
       */
      virtual void configure_beam(const ska::pst::common::AsciiHeader& beam_config);

      /**
       * @brief Prepare the UDPFormat once the complete header has been provided.
       * Includes the fixed and dynamic configuration parameters.
       *
       * @param scan_config complete set of meta-data for the observation
       */
      void configure_scan(const ska::pst::common::AsciiHeader& scan_config);

      /**
       * @brief Conclude the format.
       * Releases any allocated resources, resets internal state.
       *
       */
      void conclude();

      /* TODO: implement on new story
      void deconfigure_scan();
      void deconfigure_beam();
      */

      /**
       * @brief Reset the Format.
       * Set the prepared state to false.
       *
       */
      void reset();

      /**
       * @brief Return the number of samples that correspond to the specified number of bytes
       *
       * @param nbytes Number of bytes to use in calculation
       * @return uint64_t Number of samples that match the specified bytes
       */
      uint64_t get_samples_for_bytes(uint64_t nbytes);

      /**
       * @brief Return the resolution of the UDPFormat which corresponds to the minimum block size.
       *
       * @return uint64_t data mininmumresolution in bytes
       */
      uint64_t get_resolution();

      /**
       * @brief Return the minimum resolution of the weights in UDPFormat.
       * Corresponds to the minimum block size for the weights.
       *
       * @return uint64_t weights minimum resolution in bytes
       */
      uint64_t get_weights_resolution();

      /**
       * @brief Encode the contents of the
       *
       * @param buf
       */
      void encode_header(char * buf);

      /**
       * @brief check whether the data stream has started by virtue of the first packet being received
       *
       * @param buffer pointer to buffer container received UDP packet payload
       * @return PacketState return value indicating status: OK, IGNORE or MALFORMED
       */
      PacketState process_stream_start(char * buffer);

      /**
       * @brief Get the start timestamp for the packet stream.
       *
       * @return ska::pst::common::Time timestamp of the start of the packet stream
       */
      ska::pst::common::Time get_start_timestamp() { return start_timestamp; };

      /**
       * @brief Decode the received UDP packet and determine it's location within the data stream
       *
       * @param buffer pointer to buffer containing received UDP packet payload
       * @param offsets offsets of the data and weights in their repsective data streams
       * @param received size of the data and weights portions of the payload
       * @return PacketState status of the received packet (e.g. OK, IGNORE, MALFORMED)
       */
      virtual PacketState decode_packet(char * buffer, data_and_weights_t * offsets, data_and_weights_t * received);

      /**
       * @brief copy the most recently received packet data to the provided buffer
       *
       * @param buffer pointer to buffer
       */
      virtual void insert_last_packet(char * data, char * weights);

      /**
       * @brief Reset all of the scales values in the weights buffer to NAN
       *
       * @param buffer pointer to the weights buffer
       * @param bufsz size of the weights buffer in bytes
       */
      void clear_scales_buffer(char * buffer, uint64_t bufsz);

      /**
       * @brief print the description of the UDP packet header
       *
       */
      void print_packet_header();

      /**
       * @brief Get the oversampling numerator of the UDP format
       *
       * @return unsigned numerator of the oversampling ratio
       */
      unsigned get_os_numerator() const { return os_numerator; };

      /**
       * @brief Get the os denominator of the UDP format
       *
       * @return unsigned denominator of the oversampling ratio
       */
      unsigned get_os_denominator() const { return os_denominator; };

      /**
       * @brief Get the CBF/PSR destination for the UDP format
       *
       * @return unsigned value of the CBF/PSR destination field
       */
      unsigned get_destination() const { return destination; };

      /**
       * @brief Get the increment psn every packet value
       *
       * @return true UDP format expects PSN to increment every packet
       * @return false UDP format does not expect PSN to increment every packet
       */
      bool get_increment_psn_every_packet() const { return increment_psn_every_packet; };

      //! return the scan configuration for the weights as a string
      ska::pst::common::AsciiHeader& get_weights_scan_config();

      //! set the expected scan id for the packet stream
      void set_scan_id(uint64_t scan_id);

      //! return true if the packet is valid, but not meant to be received by this format
      inline bool is_misdirected();

    protected:

      /**
       * @brief Get the expected NBIT value for UDPFormat
       *
       * @return the expected NBIT value for the format.
       */
      virtual unsigned get_expected_nbit() = 0;

      /**
       * @brief Get the expected OS_FACTOR for the UDPFormat
       *
       * @return a pair of unsigned values.  The first is the numerator and the
       *      second is the denominator.
       */
      virtual std::pair<unsigned, unsigned> get_expected_os_factor() = 0;

      /**
       * @brief Check that the \ref beam_configured attribute matches the expected value
       *
       * @throw std::runtime_error if the beam_configured attribute does not match the expected value
       * @param expected expected value of the beam_configured attribute
       */
      inline void check_beam_configured(bool expected);

      /**
       * @brief Check that the \ref scan_configured attribute matches the expected value
       *
       * @throw std::runtime_error if the scan_configured attribute does not match the expected value
       * @param expected expected value of the scan_configured attribute
       */
      inline void check_scan_configured(bool expected);

      /**
       * @brief Check that the \ref scan_started attribute matches the expected value
       *
       * @throw std::runtime_error if the scan_started attribute does not match the expected value
       * @param expected expected value of the scan_started attribute
       */
      inline void check_scan_started(bool expected);

      //! struct of pointers to the header, weights and data of the current UDP packet.
      cbf_psr_packet_t packet{nullptr, nullptr, nullptr};

      //! the beam number or beam identifier
      unsigned beam_id{0};

      //! the scan identifier
      uint64_t scan_id{0};

      //! number of antennae
      unsigned nant{0};

      //! number of beams
      unsigned nbeam{0};

      //! number of dimensions
      unsigned ndim{0};

      //! number of polarisations
      unsigned npol{0};

      //! number of bits per dimension
      unsigned nbit{0};

      //! oversampling numerator
      unsigned os_numerator{0};

      //! oversampling denominator
      unsigned os_denominator{0};

      //! first channel
      unsigned start_channel{0};

      //! last channel
      unsigned end_channel{0};

      //! number of channels
      unsigned nchan{0};

      //! the sampling interval in microseconds
      double tsamp{0};

      //! the bandwidth in MHz
      double bw{0};

      //! the centre frequency in MHz
      double freq{0};

      //! the data rate in bytes per second
      uint64_t bytes_per_second{0};

      //! flag for format configuration, fixed time parameters received
      bool beam_configured{false};

      //! flag for format preparation, fixed and runtime parameters received
      bool scan_configured{false};

      //! flag for the starting of the scan, specifically once a packet has been received and the start_psn attribute is set
      bool scan_started{false};

      //! Number of bytes per channel in UDPFormat data
      unsigned data_channel_stride{0};

      //! Number of bytes that span the data time samples associated with all packets of the sample Packet Sequence Number (PSN)
      unsigned data_psn_stride{0};

      //! Number of bytes per channel in UDPFormat weights
      unsigned weights_channel_stride{0};

      //! Number of bytes that span the weights time samples associated with all packets of the sample Packet Sequence Number (PSN)
      unsigned weights_psn_stride{0};

      uint32_t destination{5};

      //! The Packet Sequence Number (PSN) that is associated with the the start of data
      int64_t start_psn{-1};

      //! The timestamp that corresponds to the start_psn
      ska::pst::common::Time start_timestamp;

      //! beam configuration for the weights stream of this format
      ska::pst::common::AsciiHeader weights_beam_config;

      //! scan configuration for the weights stream of this format
      ska::pst::common::AsciiHeader weights_scan_config;

      //! number of attoseconds per packet
      uint64_t attoseconds_per_packet{0};

      //! increment the PSN on every single packet, only true in LowTestVector
      bool increment_psn_every_packet{false};

    private:

      //! additional padding required for weights/data alignment
      unsigned packet_weights_size_remainder{0};

  };

} // ska::pst::recv

#endif
