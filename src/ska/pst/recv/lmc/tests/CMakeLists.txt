include_directories(../..)

add_executable(RecvLmcServiceHandlerTest src/RecvLmcServiceHandlerTest.cpp)
add_executable(RecvLmcServiceIntegrationTest src/RecvLmcServiceIntegrationTest.cpp)

set(TEST_LINK_LIBS gtest_main ${SkaPstSmrb_LIBRARIES} ${PSRDADA_LIBRARIES} ska-pst-recv-network ska-pst-recv-clients ska-pst-recv-formats ska-pst-recv-lmc ska-pst-recv-testutils ska_pst_common)

target_link_libraries(RecvLmcServiceHandlerTest ${TEST_LINK_LIBS})
target_link_libraries(RecvLmcServiceIntegrationTest ${TEST_LINK_LIBS})

add_test(RecvLmcServiceHandlerTest RecvLmcServiceHandlerTest --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
add_test(RecvLmcServiceIntegrationTest RecvLmcServiceIntegrationTest --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
