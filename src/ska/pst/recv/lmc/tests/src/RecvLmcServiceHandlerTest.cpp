/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/formats/UDPFormatFactory.h"
#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/common/lmc/LmcServiceException.h"
#include "ska/pst/recv/lmc/tests/RecvLmcServiceHandlerTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv); //NOLINT
}

namespace ska::pst::recv::test {

RecvLmcServiceHandlerTest::RecvLmcServiceHandlerTest()
    : ::testing::Test(),
    db_key("dddd"),
    wb_key("eeee"),
    db(db_key),
    wb(wb_key) // NOLINT
{
  sock = std::make_shared<UDPSocketReceive>(); //NOLINT
  _scan_id = uint64_t(time(nullptr)); //NOLINT
}

void RecvLmcServiceHandlerTest::SetUp() // NOLINT
{
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::SetUp()"); //NOLINT

  format = UDPFormatFactory("LowTestVector"); //NOLINT
  config.load_from_file(test_data_file("config.txt")); //NOLINT
  format->configure_beam(config); //NOLINT

  uint64_t data_resolution = format->get_resolution(); //NOLINT
  uint64_t weights_resolution = format->get_weights_resolution(); //NOLINT

  db.create(header_nbufs, header_bufsz, data_nbufs, data_resolution, 1, -1); //NOLINT
  wb.create(header_nbufs, header_bufsz, weights_nbufs, weights_resolution, 1, -1); //NOLINT

  SPDLOG_TRACE("RecvLmcServiceHandlerTest::SetUp creating receiver."); //NOLINT
  _recv = std::make_shared<ska::pst::recv::test::LmcServiceTestReceiver>(sock, config.get_val("DATA_HOST")); //NOLINT

  SPDLOG_TRACE("RecvLmcServiceHandlerTest::SetUp creating shared data block manager"); //NOLINT
  handler = std::make_shared<ska::pst::recv::RecvLmcServiceHandler>(_recv); //NOLINT

  SPDLOG_TRACE("RecvLmcServiceHandlerTest::SetUp() complete"); //NOLINT
}

void RecvLmcServiceHandlerTest::TearDown() // NOLINT
{
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::TearDown()"); //NOLINT
  _recv->reset_throw_exception(); //NOLINT
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::TearDown() _recv->reset_throw_exception() done"); //NOLINT
  _recv->quit(); //NOLINT
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::TearDown() _recv->quit() done"); //NOLINT
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::TearDown() _recv=nullptr done"); //NOLINT
  db.destroy(); //NOLINT
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::TearDown() db.destroy() done"); //NOLINT
  wb.destroy(); //NOLINT
  SPDLOG_TRACE("RecvLmcServiceHandlerTest::TearDown() wb.destroy() done"); //NOLINT
}

void RecvLmcServiceHandlerTest::config_as_resources(ska::pst::lmc::ReceiveBeamConfiguration *request) // NOLINT
{
  auto subband_resources = request->mutable_subband_resources(); //NOLINT

  request->set_udp_nsamp(config.get_uint32("UDP_NSAMP")); //NOLINT
  request->set_wt_nsamp(config.get_uint32("WT_NSAMP")); //NOLINT
  request->set_udp_nchan(config.get_uint32("UDP_NCHAN")); //NOLINT
  request->set_frontend(config.get_val("FRONTEND")); //NOLINT
  request->set_fd_poln(config.get_val("FD_POLN")); //NOLINT
  request->set_fd_hand(config.get_int32("FD_HAND")); //NOLINT
  request->set_fd_sang(config.get_uint32("FD_SANG")); //NOLINT
  request->set_fd_mode(config.get_val("FD_MODE")); //NOLINT
  request->set_fa_req(config.get_float("FA_REQ")); //NOLINT
  request->set_nant(config.get_uint32("NANT")); //NOLINT
  request->set_antennas(config.get_val("ANTENNAE")); //NOLINT
  request->set_ant_weights(config.get_val("ANT_WEIGHTS")); //NOLINT
  request->set_npol(config.get_uint32("NPOL")); //NOLINT
  request->set_nbits(config.get_uint32("NBIT")); //NOLINT
  request->set_ndim(config.get_uint32("NDIM")); //NOLINT
  request->set_tsamp(config.get_double("TSAMP")); //NOLINT
  request->set_ovrsamp(config.get_val("OS_FACTOR")); //NOLINT
  request->set_nsubband(config.get_uint32("NSUBBAND")); //NOLINT
  request->set_udp_format(config.get_val("UDP_FORMAT")); //NOLINT
  request->set_bytes_per_second(config.get_double("BYTES_PER_SECOND")); //NOLINT
  request->set_beam_id(config.get_val("BEAM_ID")); //NOLINT
  subband_resources->set_data_key(config.get_val("DATA_KEY")); //NOLINT
  subband_resources->set_weights_key(config.get_val("WEIGHTS_KEY")); //NOLINT
  subband_resources->set_bandwidth(config.get_double("BW")); //NOLINT
  subband_resources->set_nchan(config.get_uint32("NCHAN")); //NOLINT
  subband_resources->set_start_channel(config.get_uint32("START_CHANNEL")); //NOLINT
  subband_resources->set_end_channel(config.get_uint32("END_CHANNEL")); //NOLINT
  subband_resources->set_frequency(config.get_double("FREQ")); //NOLINT
  subband_resources->set_start_channel_out(config.get_uint32("START_CHANNEL_OUT")); //NOLINT
  subband_resources->set_end_channel_out(config.get_uint32("END_CHANNEL_OUT")); //NOLINT
  subband_resources->set_nchan_out(config.get_uint32("NCHAN_OUT")); //NOLINT
  subband_resources->set_bandwidth_out(config.get_double("BW_OUT")); //NOLINT
  subband_resources->set_frequency_out(config.get_double("FREQ_OUT")); //NOLINT
  subband_resources->set_data_host(config.get_val("DATA_HOST")); //NOLINT
  subband_resources->set_data_port(config.get_uint32("DATA_PORT")); //NOLINT
}


void RecvLmcServiceHandlerTest::config_as_scan_configuration(ska::pst::lmc::ReceiveScanConfiguration *configuration) // NOLINT
{
    configuration->set_activation_time(config.get_val("ACTIVATION_TIME")); //NOLINT
    configuration->set_observer(config.get_val("OBSERVER")); //NOLINT
    configuration->set_projid(config.get_val("PROJID")); //NOLINT
    configuration->set_pnt_id(config.get_val("PNT_ID")); //NOLINT
    configuration->set_subarray_id(config.get_val("SUBARRAY_ID")); //NOLINT
    configuration->set_source(config.get_val("SOURCE")); //NOLINT
    configuration->set_itrf(config.get_val("ITRF")); //NOLINT
    if (config.has("BMAJ"))
    {
        configuration->set_bmaj(config.get_float("BMAJ")); //NOLINT
    }
    if (config.has("BMIN"))
    {
        configuration->set_bmin(config.get_float("BMIN")); //NOLINT
    }
    configuration->set_coord_md(config.get_val("COORD_MD")); //NOLINT
    configuration->set_equinox(config.get_val("EQUINOX")); //NOLINT
    configuration->set_stt_crd1(config.get_val("STT_CRD1")); //NOLINT
    configuration->set_stt_crd2(config.get_val("STT_CRD2")); //NOLINT
    configuration->set_trk_mode(config.get_val("TRK_MODE")); //NOLINT
    configuration->set_scanlen_max(config.get_int32("SCANLEN_MAX")); //NOLINT
    if (config.has("TEST_VECTOR"))
    {
        configuration->set_test_vector(config.get_val("TEST_VECTOR")); //NOLINT
    }
    configuration->set_execution_block_id(config.get_val("EB_ID"));
}

void RecvLmcServiceHandlerTest::validate_beam_configuration()
{
    ska::pst::lmc::BeamConfiguration resources; //NOLINT
    config_as_resources(resources.mutable_receive()); //NOLINT

    validate_beam_configuration(resources);
}

void RecvLmcServiceHandlerTest::validate_beam_configuration(const ska::pst::lmc::BeamConfiguration &resources)
{
    handler->validate_beam_configuration(resources);
}

void RecvLmcServiceHandlerTest::configure_beam() // NOLINT
{
    ska::pst::lmc::BeamConfiguration resources; //NOLINT
    config_as_resources(resources.mutable_receive()); //NOLINT

    configure_beam(resources); //NOLINT
}

void RecvLmcServiceHandlerTest::configure_beam(const ska::pst::lmc::BeamConfiguration &resources) // NOLINT
{
    handler->configure_beam(resources); //NOLINT
}

void RecvLmcServiceHandlerTest::validate_scan_configuration()
{
    ska::pst::lmc::ScanConfiguration scan_configuration; //NOLINT
    config_as_scan_configuration(scan_configuration.mutable_receive()); //NOLINT

    validate_scan_configuration(scan_configuration);
}

void RecvLmcServiceHandlerTest::validate_scan_configuration(const ska::pst::lmc::ScanConfiguration &config)
{
    handler->validate_scan_configuration(config);
}

void RecvLmcServiceHandlerTest::configure_scan() // NOLINT
{
    ska::pst::lmc::ScanConfiguration scan_configuration; //NOLINT
    auto resources_msg = scan_configuration.mutable_receive(); //NOLINT
    config_as_scan_configuration(resources_msg); //NOLINT

    configure_scan(scan_configuration); //NOLINT
}

void RecvLmcServiceHandlerTest::configure_scan(const ska::pst::lmc::ScanConfiguration &scan_configuration) // NOLINT
{
    handler->configure_scan(scan_configuration); //NOLINT
}

void RecvLmcServiceHandlerTest::start_scan(uint64_t scan_id) // NOLINT
{
    ska::pst::lmc::StartScanRequest request; //NOLINT
    request.set_scan_id(scan_id); //NOLINT
    handler->start_scan(request); //NOLINT
}

auto RecvLmcServiceHandlerTest::generate_scan_id() -> uint64_t
{
    _scan_id++; //NOLINT
    return _scan_id; //NOLINT
}

LmcServiceTestReceiver::~LmcServiceTestReceiver() // NOLINT
{
    SPDLOG_TRACE("LmcServiceTestReceiver::~LmcServiceTestReceiver"); //NOLINT
}

void LmcServiceTestReceiver::configure_beam(
    const ska::pst::common::AsciiHeader &config
) // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Configure beam error."); //NOLINT
    }
    UDPReceiveDB::configure_beam(config); //NOLINT
}

void LmcServiceTestReceiver::deconfigure_beam() // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Deconfigure beam error."); //NOLINT
    }
    UDPReceiveDB::deconfigure_beam(); //NOLINT
}

auto LmcServiceTestReceiver::get_beam_configuration() -> ska::pst::common::AsciiHeader&
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Getting beam configuration error."); //NOLINT
    }
    return UDPReceiveDB::get_beam_configuration(); //NOLINT
}

void LmcServiceTestReceiver::configure_scan(
    const ska::pst::common::AsciiHeader &config
) // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Configure scan error."); //NOLINT
    }
    UDPReceiveDB::configure_scan(config); //NOLINT
}

void LmcServiceTestReceiver::deconfigure_scan() // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Deconfigure scan error."); //NOLINT
    }
    UDPReceiveDB::deconfigure_scan(); //NOLINT
}

auto LmcServiceTestReceiver::get_scan_configuration() -> ska::pst::common::AsciiHeader&
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Get scan configuration error."); //NOLINT
    }
    return UDPReceiveDB::get_scan_configuration(); //NOLINT
}

void LmcServiceTestReceiver::start_scan(const ska::pst::common::AsciiHeader &config) // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Start scan error."); //NOLINT
    }
    UDPReceiveDB::start_scan(config); //NOLINT
}

void LmcServiceTestReceiver::stop_scan() // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Stop scan error."); //NOLINT
    }
    UDPReceiveDB::stop_scan(); //NOLINT
}

void LmcServiceTestReceiver::throw_exception_on_next_call() // NOLINT
{
    _throw_on_next_call = true; //NOLINT
}

void LmcServiceTestReceiver::reset_throw_exception() // NOLINT
{
    _throw_on_next_call = false; //NOLINT
}



TEST_F(RecvLmcServiceHandlerTest, configure_beam) // NOLINT
{
    EXPECT_EQ(ska::pst::common::State::Idle, _recv->get_state()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam - configuring beam"); //NOLINT
    configure_beam(); //NOLINT

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_recv->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam - getting beam configuration"); //NOLINT
    ska::pst::lmc::BeamConfiguration get_response; //NOLINT
    handler->get_beam_configuration(&get_response); //NOLINT
    EXPECT_TRUE(get_response.has_receive()); // NOLINT

    auto get_response_receive = get_response.receive(); //NOLINT

    ska::pst::lmc::ReceiveBeamConfiguration expected_resources; //NOLINT
    config_as_resources(&expected_resources); //NOLINT

    EXPECT_EQ(get_response_receive.DebugString(), expected_resources.DebugString()); //NOLINT

    auto &resources = _recv->get_beam_configuration(); //NOLINT
    ASSERT_EQ(config.get_uint32("UDP_NSAMP"), resources.get_uint32("UDP_NSAMP")); //NOLINT
    ASSERT_EQ(config.get_uint32("WT_NSAMP"), resources.get_uint32("WT_NSAMP")); //NOLINT
    ASSERT_EQ(config.get_uint32("UDP_NCHAN"), resources.get_uint32("UDP_NCHAN")); //NOLINT
    ASSERT_EQ(config.get_val("FRONTEND"), resources.get_val("FRONTEND")); //NOLINT
    ASSERT_EQ(config.get_val("FD_POLN"), resources.get_val("FD_POLN")); //NOLINT
    ASSERT_EQ(config.get_int32("FD_HAND"), resources.get_int32("FD_HAND")); //NOLINT
    ASSERT_EQ(config.get_uint32("FD_SANG"), resources.get_uint32("FD_SANG")); //NOLINT
    ASSERT_EQ(config.get_val("FD_MODE"), resources.get_val("FD_MODE")); //NOLINT
    ASSERT_EQ(config.get_float("FA_REQ"), resources.get_float("FA_REQ")); //NOLINT
    ASSERT_EQ(config.get_uint32("NANT"), resources.get_uint32("NANT")); //NOLINT
    ASSERT_EQ(config.get_val("ANTENNAE"), resources.get_val("ANTENNAE")); //NOLINT
    ASSERT_EQ(config.get_val("ANT_WEIGHTS"), resources.get_val("ANT_WEIGHTS")); //NOLINT
    ASSERT_EQ(config.get_uint32("NPOL"), resources.get_uint32("NPOL")); //NOLINT
    ASSERT_EQ(config.get_uint32("NBIT"), resources.get_uint32("NBIT")); //NOLINT
    ASSERT_EQ(config.get_uint32("NDIM"), resources.get_uint32("NDIM")); //NOLINT
    ASSERT_EQ(config.get_double("TSAMP"), resources.get_double("TSAMP")); //NOLINT
    ASSERT_EQ(config.get_val("OS_FACTOR"), resources.get_val("OS_FACTOR")); //NOLINT
    ASSERT_EQ(config.get_uint32("NSUBBAND"), resources.get_uint32("NSUBBAND")); //NOLINT
    ASSERT_EQ(config.get_val("UDP_FORMAT"), resources.get_val("UDP_FORMAT")); //NOLINT
    ASSERT_EQ(config.get_val("DATA_KEY"), resources.get_val("DATA_KEY")); //NOLINT
    ASSERT_EQ(config.get_val("WEIGHTS_KEY"), resources.get_val("WEIGHTS_KEY")); //NOLINT
    ASSERT_EQ(config.get_double("BW"), resources.get_double("BW")); //NOLINT
    ASSERT_EQ(config.get_uint32("NCHAN"), resources.get_uint32("NCHAN")); //NOLINT
    ASSERT_EQ(config.get_uint32("START_CHANNEL"), resources.get_uint32("START_CHANNEL")); //NOLINT
    ASSERT_EQ(config.get_uint32("END_CHANNEL"), resources.get_uint32("END_CHANNEL")); //NOLINT
    ASSERT_EQ(config.get_double("FREQ"), resources.get_double("FREQ")); //NOLINT
    ASSERT_EQ(config.get_uint32("START_CHANNEL_OUT"), resources.get_uint32("START_CHANNEL_OUT")); //NOLINT
    ASSERT_EQ(config.get_uint32("END_CHANNEL_OUT"), resources.get_uint32("END_CHANNEL_OUT")); //NOLINT
    ASSERT_EQ(config.get_uint32("NCHAN_OUT"), resources.get_uint32("NCHAN_OUT")); //NOLINT
    ASSERT_EQ(config.get_double("BW_OUT"), resources.get_double("BW_OUT")); //NOLINT
    ASSERT_EQ(config.get_double("FREQ_OUT"), resources.get_double("FREQ_OUT")); //NOLINT
    ASSERT_EQ(config.get_double("BYTES_PER_SECOND"), resources.get_double("BYTES_PER_SECOND")); //NOLINT
    ASSERT_EQ(config.get_val("BEAM_ID"), resources.get_val("BEAM_ID")); //NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, _recv->get_state()); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam - deconfiguring beam"); //NOLINT
    handler->deconfigure_beam(); //NOLINT
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_recv->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam - beam deconfigured"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, configure_beam_when_already_configured_for_beam) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam_when_already_configured_for_beam - configuring beam"); //NOLINT
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam(); //NOLINT

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_recv->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam_when_already_configured_for_beam - beam deconfigured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam_when_already_configured_for_beam - attempting to reconfigure beam"); //NOLINT
    try {
        configure_beam(); //NOLINT
        FAIL() << " expected exception when trying to reassing resources."; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Resources already assigned for RECV."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam_when_already_configured_for_beam - beam configured already"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, configure_beam_when_no_receive_request_exists) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_beam_when_no_receive_request_exists - configuring beam with invalid request"); //NOLINT
    auto beam_configuration = ska::pst::lmc::BeamConfiguration(); //NOLINT
    beam_configuration.mutable_test(); //NOLINT

    try {
        handler->configure_beam(beam_configuration); //NOLINT
        FAIL() << " expected configure_beam to throw exception due not having RECV field.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Expected a RECV beam configuration object, but none were provided."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT); //NOLINT
    }

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_when_no_receive_configuration_exists - failed to configure beam"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, get_beam_configuration_when_not_beam_configured) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::get_beam_configuration_when_not_beam_configured - getting beam configuration"); //NOLINT
    EXPECT_FALSE(handler->is_beam_configured()); //NOLINT

    ska::pst::lmc::BeamConfiguration response; //NOLINT
    try {
        handler->get_beam_configuration(&response); //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV not configured for beam."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::get_beam_configuration_when_not_beam_configured - beam not configured"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, release_assigned_resources_when_not_beam_configured) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::release_assigned_resources_when_not_beam_configured - deconfiguring beam"); //NOLINT
    EXPECT_EQ(ska::pst::common::State::Idle, _recv->get_state()); //NOLINT
    EXPECT_FALSE(handler->is_beam_configured()); //NOLINT

    ska::pst::lmc::BeamConfiguration response; //NOLINT
    try {
        handler->deconfigure_beam(); //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV not configured for beam."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::release_assigned_resources_when_not_beam_configured - beam not configured"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, configure_deconfigure) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - configuring beam"); //NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - configuring scan"); //NOLINT
    EXPECT_FALSE(handler->is_scan_configured()); //NOLINT
    configure_scan(); //NOLINT
    EXPECT_TRUE(handler->is_scan_configured()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - scan configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - getting configuration"); //NOLINT
    ska::pst::lmc::ScanConfiguration get_response; //NOLINT
    handler->get_scan_configuration(&get_response); //NOLINT
    EXPECT_TRUE(get_response.has_receive()); // NOLINT

    auto get_response_receive = get_response.receive(); //NOLINT

    ASSERT_EQ(config.get_val("ACTIVATION_TIME"), get_response_receive.activation_time()); //NOLINT
    ASSERT_EQ(config.get_val("OBSERVER"), get_response_receive.observer()); //NOLINT
    ASSERT_EQ(config.get_val("PROJID"), get_response_receive.projid()); //NOLINT
    ASSERT_EQ(config.get_val("PNT_ID"), get_response_receive.pnt_id()); //NOLINT
    ASSERT_EQ(config.get_val("SUBARRAY_ID"), get_response_receive.subarray_id()); //NOLINT
    ASSERT_EQ(config.get_val("SOURCE"), get_response_receive.source()); //NOLINT
    ASSERT_EQ(config.get_val("ITRF"), get_response_receive.itrf()); //NOLINT
    ASSERT_EQ(config.get_val("COORD_MD"), get_response_receive.coord_md()); //NOLINT
    ASSERT_EQ(config.get_val("EQUINOX"), get_response_receive.equinox()); //NOLINT
    ASSERT_EQ(config.get_val("STT_CRD1"), get_response_receive.stt_crd1()); //NOLINT
    ASSERT_EQ(config.get_val("STT_CRD2"), get_response_receive.stt_crd2()); //NOLINT
    ASSERT_EQ(config.get_val("TRK_MODE"), get_response_receive.trk_mode()); //NOLINT
    ASSERT_EQ(config.get_int32("SCANLEN_MAX"), get_response_receive.scanlen_max()); //NOLINT
    ASSERT_EQ(config.get_val("EB_ID"), get_response_receive.execution_block_id()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - checked configuration"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - deconfiguring scan"); //NOLINT
    handler->deconfigure_scan(); //NOLINT
    EXPECT_FALSE(handler->is_scan_configured()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - scan deconfigured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - deconfiguring beam"); //NOLINT
    handler->deconfigure_beam(); //NOLINT
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure - beam deconfigured"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, configure_deconfigure_optional_values) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - configuring beam"); //NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - configuring scan"); //NOLINT
    config.set_val("BMAJ", "1.0"); //NOLINT
    config.set_val("BMIN", "2.0"); //NOLINT
    config.set("TEST_VECTOR", "test_vector"); //NOLINT
    configure_scan(); //NOLINT
    EXPECT_TRUE(handler->is_scan_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - scan configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - getting configuration"); //NOLINT
    ska::pst::lmc::ScanConfiguration get_response; //NOLINT
    handler->get_scan_configuration(&get_response); //NOLINT
    EXPECT_TRUE(get_response.has_receive()); // NOLINT

    const auto &scan_configuration = get_response.receive(); //NOLINT
    EXPECT_EQ(scan_configuration.bmaj(), 1.0f); //NOLINT
    EXPECT_EQ(scan_configuration.bmin(), 2.0f); //NOLINT
    EXPECT_EQ(scan_configuration.test_vector(), "test_vector"); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - checked configuration"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - deconfiguring scan"); //NOLINT
    handler->deconfigure_scan(); //NOLINT
    EXPECT_FALSE(handler->is_scan_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - scan deconfigured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - deconfiguring beam"); //NOLINT
    handler->deconfigure_beam(); //NOLINT
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_recv->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_deconfigure_optional_values - beam deconfigured"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, configure_when_not_beam_configured) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_when_not_beam_configured - configuring scan"); //NOLINT

    try {
        configure_scan(); //NOLINT
        FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV not configured for beam."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, configure_scan_when_scan_already_configured) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_scan_when_scan_already_configured - configuring beam"); //NOLINT

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam(); //NOLINT

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_scan_when_scan_already_configured - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_scan_when_scan_already_configured - configuring scan"); //NOLINT
    EXPECT_FALSE(handler->is_scan_configured()); //NOLINT
    configure_scan(); //NOLINT
    EXPECT_TRUE(handler->is_scan_configured()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_scan_when_scan_already_configured - scan configured"); //NOLINT

    try {
        configure_scan(); //NOLINT
        FAIL() << " expected configure_scan to throw exception due to already configured.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Scan already configured for RECV."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, configure_should_have_recv_object) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_should_have_recv_object - configuring beam"); //NOLINT

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_should_have_recv_object - configuring beam"); //NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_should_have_recv_object - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::configure_should_have_recv_object - configuring scan"); //NOLINT
    try {
        ska::pst::lmc::ScanConfiguration configuration; //NOLINT
        configuration.mutable_test(); //NOLINT
        handler->configure_scan(configuration); //NOLINT
        FAIL() << " expected configure_scan to throw exception due not having RECV field.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Expected a RECV scan configuration object, but none were provided."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, deconfigure_scan_when_scan_not_configured) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::deconfigure_scan_when_scan_not_configured - deconfiguring scan"); //NOLINT

    try {
        handler->deconfigure_scan(); //NOLINT
        FAIL() << " expected deconfigure_scan to throw exception due to not having beam configured.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV not configured for scan."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, get_scan_configuration_when_scan_not_configured) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::get_scan_configuration_when_scan_not_configured - getting scan configuration"); // NOLINT

    try {
        ska::pst::lmc::ScanConfiguration scan_configuration; //NOLINT
        handler->get_scan_configuration(&scan_configuration); //NOLINT
        FAIL() << " expected get_scan_configuration to throw exception due to scan not configured.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV not configured for scan."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, start_scan_stop_scan) // NOLINT
{

    EXPECT_EQ(ska::pst::common::State::Idle, _recv->get_state()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - configuring beam"); //NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - configuring scan"); //NOLINT
    configure_scan(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - scan configured"); //NOLINT

    uint64_t scan_id = generate_scan_id(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - starting scan with SCAN_ID={}", scan_id); //NOLINT
    EXPECT_FALSE(handler->is_scanning()); //NOLINT
    start_scan(scan_id); //NOLINT
    EXPECT_EQ(ska::pst::common::State::Scanning, _recv->get_state()); //NOLINT
    EXPECT_TRUE(_recv->is_scanning()); //NOLINT
    EXPECT_TRUE(handler->is_scanning()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - scanning"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - ending scan"); //NOLINT
    handler->stop_scan(); //NOLINT
    EXPECT_EQ(ska::pst::common::State::ScanConfigured, _recv->get_state()); //NOLINT
    EXPECT_FALSE(_recv->is_scanning()); //NOLINT
    EXPECT_FALSE(handler->is_scanning()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::start_scan_stop_scan - stopped scanning"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, start_scan_when_scan_not_configured_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - configuring beam"); //NOLINT

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - start scan"); //NOLINT
    EXPECT_FALSE(handler->is_scan_configured()); //NOLINT

    try {
        uint64_t scan_id = generate_scan_id(); //NOLINT
        start_scan(scan_id); //NOLINT
        FAIL() << " expected configure to throw exception due to not being configured.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV not configured for scan."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, start_scan_again_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - configuring beam"); //NOLINT

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - beam configured"); //NOLINT

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - configuring scan"); //NOLINT
    configure_scan(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - scan configured"); //NOLINT

    uint64_t scan_id = generate_scan_id(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - start scan"); //NOLINT
    EXPECT_FALSE(handler->is_scanning()); //NOLINT
    start_scan(scan_id); //NOLINT
    EXPECT_TRUE(handler->is_scanning()); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::scan_again_should_throw_exception - scanning"); //NOLINT

    try {
        scan_id = generate_scan_id(); //NOLINT
        start_scan(scan_id); //NOLINT
        FAIL() << " expected configure to throw exception due to already scanning.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "RECV is already scanning."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, stop_scan_when_not_scanning_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::stop_scan_when_not_scanning_should_throw_exception - end scan"); //NOLINT
    EXPECT_FALSE(handler->is_scanning()); //NOLINT

    try {
        handler->stop_scan(); //NOLINT
        FAIL() << " expected configure to throw exception due to not currently scanning.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Received stop_scan request when RECV is not scanning."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, get_monitor_data) // NOLINT
{
    // configure beam
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - configuring beam"); //NOLINT
    configure_beam(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - beam configured"); //NOLINT

    // configure
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - configuring scan"); //NOLINT
    configure_scan(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - scan configured"); //NOLINT

    std::shared_ptr<UDPStats> fake_stats = std::make_shared<UDPStats>(); //NOLINT
    _recv->set_stats(fake_stats); //NOLINT

    // increment number of packets
    static constexpr uint64_t fake_nbytes = 10240; //NOLINT
    fake_stats->increment(fake_nbytes); //NOLINT
    static constexpr uint64_t fake_npackets = 10; //NOLINT
    fake_stats->dropped(fake_npackets); //NOLINT
    fake_stats->misdirected(fake_npackets);
    fake_stats->malformed(fake_npackets);
    fake_stats->misordered(fake_npackets);
    fake_stats->compute_rates(); //NOLINT

    // scan
    uint64_t scan_id = generate_scan_id(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - starting scan"); //NOLINT
    start_scan(scan_id); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - scanning"); //NOLINT

    // monitor
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - get monitor data"); //NOLINT
    ska::pst::lmc::MonitorData monitor_data; //NOLINT
    handler->get_monitor_data(&monitor_data); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - get monitor data"); //NOLINT

    const auto &receive_monitor_data = monitor_data.receive(); //NOLINT

    EXPECT_FLOAT_EQ(receive_monitor_data.receive_rate(), fake_stats->get_data_transmission_rate()); //NOLINT
    EXPECT_EQ(receive_monitor_data.data_received(), fake_stats->get_data_transmitted()); //NOLINT
    EXPECT_FLOAT_EQ(receive_monitor_data.data_drop_rate(), fake_stats->get_data_drop_rate()); //NOLINT
    EXPECT_EQ(receive_monitor_data.data_dropped(), fake_stats->get_data_dropped()); //NOLINT
    EXPECT_EQ(receive_monitor_data.misdirected_packets(), fake_stats->get_misdirected()); //NOLINT
    EXPECT_EQ(receive_monitor_data.malformed_packets(), fake_stats->get_malformed()); //NOLINT
    EXPECT_EQ(receive_monitor_data.misordered_packets(), fake_stats->get_misordered()); //NOLINT

    // end scan
    handler->stop_scan(); //NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::monitor_should_report_read_write_stats_correctly - scan stopped"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, get_monitor_data_when_not_scanning_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::get_monitor_data_when_not_scanning_should_throw_exception - stop scan"); //NOLINT
    EXPECT_FALSE(handler->is_scanning()); //NOLINT

    try {
        ska::pst::lmc::MonitorData monitor_data; //NOLINT
        handler->get_monitor_data(&monitor_data); //NOLINT
        FAIL() << " expected get_monitor_data to throw exception due to not currently scanning.\n"; //NOLINT
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Received get_monitor_data request when RECV is not scanning."); //NOLINT
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING); //NOLINT
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION); //NOLINT
    }
}

TEST_F(RecvLmcServiceHandlerTest, get_env) // NOLINT
{
    ska::pst::lmc::GetEnvironmentResponse response; //NOLINT
    handler->get_env(&response); //NOLINT

    EXPECT_EQ(response.values().size(), 2); //NOLINT
    auto values = response.values(); //NOLINT

    EXPECT_EQ(values.count("data_host"), 1); //NOLINT
    EXPECT_TRUE(values["data_host"].has_string_value()); //NOLINT
    EXPECT_EQ(values["data_host"].string_value(), _recv->get_data_host()); //NOLINT

    EXPECT_EQ(values.count("data_port"), 1); //NOLINT
    EXPECT_TRUE(values["data_port"].has_signed_int_value()); //NOLINT
    EXPECT_EQ(values["data_port"].signed_int_value(), _recv->get_data_port()); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, go_to_runtime_error) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::go_to_runtime_error");

    try {
        throw std::runtime_error("this is a test error");
    } catch (...) {
        handler->go_to_runtime_error(std::current_exception());
    }

    ASSERT_EQ(ska::pst::common::RuntimeError, _recv->get_state());
    ASSERT_EQ(ska::pst::common::RuntimeError, handler->get_application_manager_state());

    try {
        if (handler->get_application_manager_exception()) {
            std::rethrow_exception(handler->get_application_manager_exception());
        } else {
            // the exception should be set and not null
            ASSERT_FALSE(true);
        }
    } catch (const std::exception& exc) {
        ASSERT_EQ("this is a test error", std::string(exc.what()));
    }
}

TEST_F(RecvLmcServiceHandlerTest, reset_non_runtime_error_state) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_non_runtime_error_state - configuring beam");
    configure_beam();
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_non_runtime_error_state - beam configured");

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_non_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_non_runtime_error_state - scan configured");

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_non_runtime_error_state - resetting");
    handler->reset();
    EXPECT_TRUE(handler->is_scan_configured());
    EXPECT_TRUE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::ScanConfigured, handler->get_application_manager_state());
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_non_runtime_error_state - reset");
}

TEST_F(RecvLmcServiceHandlerTest, reset_from_runtime_error_state) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_from_runtime_error_state - configuring beam");
    configure_beam();
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_from_runtime_error_state - beam configured");

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_from_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_from_runtime_error_state - scan configured");

    try {
        throw std::runtime_error("force fault state");
    } catch (...) {
        handler->go_to_runtime_error(std::current_exception());
    }

    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_from_runtime_error_state - resetting");
    handler->reset();
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::Idle, handler->get_application_manager_state());
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::reset_from_runtime_error_state - reset");
}

TEST_F(RecvLmcServiceHandlerTest, validate_beam_configuration) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_beam_configuration - validating beam config"); //NOLINT
    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT
    validate_beam_configuration(); //NOLINT
    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_beam_configuration - beam config validated"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, validate_beam_configuration_with_invalid_config) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_beam_configuration_with_invalid_config - validating beam config"); //NOLINT
    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT

    config.set("DATA_KEY", "@!00");

    EXPECT_THROW(validate_beam_configuration(), ska::pst::common::pst_validation_error); // NOLINT

    // reset invalid key
    config.set("DATA_KEY", "a000");
    config.set("NBIT", 42);  // NOLINT

    EXPECT_THROW(validate_beam_configuration(), ska::pst::common::pst_validation_error); // NOLINT

    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_beam_configuration_with_invalid_config - beam config validated"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, validate_scan_configuration) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_scan_configuration - validating beam config"); //NOLINT
    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT
    validate_scan_configuration(); //NOLINT
    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_scan_configuration - beam config validated"); //NOLINT
}

TEST_F(RecvLmcServiceHandlerTest, validate_scan_configuration_with_invalid_source) // NOLINT
{
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_scan_configuration_with_invalid_source - validating scan config"); //NOLINT
    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT

    ska::pst::lmc::ScanConfiguration scan_config; //NOLINT
    auto recv_scan_config = scan_config.mutable_receive();
    config_as_scan_configuration(recv_scan_config);
    recv_scan_config->set_source("Invalid source#!");

    EXPECT_THROW(validate_scan_configuration(scan_config), ska::pst::common::pst_validation_error); // NOLINT

    EXPECT_EQ(_recv->get_state(), ska::pst::common::State::Idle); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceHandlerTest::validate_scan_configuration_with_invalid_source - scan config validated"); //NOLINT
}

} // namespace ska::pst::recv::test