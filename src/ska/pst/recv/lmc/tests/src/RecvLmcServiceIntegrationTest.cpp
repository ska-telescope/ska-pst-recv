/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/recv/definitions.h"
#include "ska/pst/recv/formats/UDPFormatFactory.h"
#include "ska/pst/recv/testutils/GtestMain.h"
#include "ska/pst/recv/lmc/tests/RecvLmcServiceIntegrationTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::recv::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::recv::test {

static constexpr bool DRY_RUN = true;

RecvLmcServiceIntegrationTest::RecvLmcServiceIntegrationTest()
    : ::testing::Test(),
    db_key("dddd"),
    wb_key("eeee"),
    db(db_key),
    wb(wb_key) // NOLINT
{
  sock = std::make_shared<UDPSocketReceive>(); // NOLINT
}

void RecvLmcServiceIntegrationTest::SetUp() // NOLINT
{
  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp()"); // NOLINT

  format = UDPFormatFactory("LowTestVector"); // NOLINT
  config.load_from_file(test_data_file("config.txt")); // NOLINT
  format->configure_beam(config); // NOLINT

  uint64_t data_resolution = format->get_resolution(); // NOLINT
  uint64_t weights_resolution = format->get_weights_resolution(); // NOLINT

  db.create(header_nbufs, header_bufsz, data_nbufs, data_resolution, 1, -1); // NOLINT
  wb.create(header_nbufs, header_bufsz, weights_nbufs, weights_resolution, 1, -1); // NOLINT

  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp creating receiver."); // NOLINT
  _recv = std::make_shared<ska::pst::recv::test::LmcServiceTestReceiver>(sock, config.get_val("DATA_HOST")); // NOLINT

  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp creating handler"); // NOLINT
  _handler = std::make_shared<ska::pst::recv::RecvLmcServiceHandler>(_recv); // NOLINT

  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp creating shared data block manager"); // NOLINT
  _service = std::make_shared<ska::pst::common::LmcService>("TEST", _handler, _port); // NOLINT

  // force getting a port set, we need gRPC to start to bind to get port.
  _service->start(); // NOLINT

  _port = _service->port(); // NOLINT

  std::string server_address("127.0.0.1:"); // NOLINT
  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp creating client connection on port {}", _port); // NOLINT
  server_address.append(std::to_string(_port)); // NOLINT
  _channel = grpc::CreateChannel(server_address, grpc::InsecureChannelCredentials()); // NOLINT
  _stub = ska::pst::lmc::PstLmcService::NewStub(_channel); // NOLINT

  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp() complete"); // NOLINT
}

void RecvLmcServiceIntegrationTest::TearDown() // NOLINT
{
  SPDLOG_TRACE("RecvLmcServiceIntegrationTest::SetUp()"); // NOLINT
  _recv->reset_throw_exception(); // NOLINT
  _recv->quit(); // NOLINT
  _service->stop(); // NOLINT
  db.destroy(); // NOLINT
  wb.destroy(); // NOLINT
}

void RecvLmcServiceIntegrationTest::config_as_resources(ska::pst::lmc::ReceiveBeamConfiguration *request) // NOLINT
{
  auto subband_resources = request->mutable_subband_resources(); // NOLINT

  request->set_udp_nsamp(config.get_uint32("UDP_NSAMP")); // NOLINT
  request->set_wt_nsamp(config.get_uint32("WT_NSAMP")); // NOLINT
  request->set_udp_nchan(config.get_uint32("UDP_NCHAN")); // NOLINT
  request->set_frontend(config.get_val("FRONTEND")); // NOLINT
  request->set_fd_poln(config.get_val("FD_POLN")); // NOLINT
  request->set_fd_hand(config.get_int32("FD_HAND")); // NOLINT
  request->set_fd_sang(config.get_uint32("FD_SANG")); // NOLINT
  request->set_fd_mode(config.get_val("FD_MODE")); // NOLINT
  request->set_fa_req(config.get_float("FA_REQ")); // NOLINT
  request->set_nant(config.get_uint32("NANT")); // NOLINT
  request->set_antennas(config.get_val("ANTENNAE")); // NOLINT
  request->set_ant_weights(config.get_val("ANT_WEIGHTS")); // NOLINT
  request->set_npol(config.get_uint32("NPOL")); // NOLINT
  request->set_nbits(config.get_uint32("NBIT")); // NOLINT
  request->set_ndim(config.get_uint32("NDIM")); // NOLINT
  request->set_tsamp(config.get_double("TSAMP")); // NOLINT
  request->set_ovrsamp(config.get_val("OS_FACTOR")); // NOLINT
  request->set_nsubband(config.get_uint32("NSUBBAND")); // NOLINT
  request->set_udp_format(config.get_val("UDP_FORMAT")); // NOLINT
  request->set_bytes_per_second(config.get_double("BYTES_PER_SECOND")); // NOLINT
  request->set_beam_id(config.get_val("BEAM_ID")); // NOLINT
  subband_resources->set_data_key(config.get_val("DATA_KEY")); // NOLINT
  subband_resources->set_weights_key(config.get_val("WEIGHTS_KEY")); // NOLINT
  subband_resources->set_bandwidth(config.get_double("BW")); // NOLINT
  subband_resources->set_nchan(config.get_uint32("NCHAN")); // NOLINT
  subband_resources->set_start_channel(config.get_uint32("START_CHANNEL")); // NOLINT
  subband_resources->set_end_channel(config.get_uint32("END_CHANNEL")); // NOLINT
  subband_resources->set_frequency(config.get_double("FREQ")); // NOLINT
  subband_resources->set_start_channel_out(config.get_uint32("START_CHANNEL_OUT")); // NOLINT
  subband_resources->set_end_channel_out(config.get_uint32("END_CHANNEL_OUT")); // NOLINT
  subband_resources->set_nchan_out(config.get_uint32("NCHAN_OUT")); // NOLINT
  subband_resources->set_bandwidth_out(config.get_double("BW_OUT")); // NOLINT
  subband_resources->set_frequency_out(config.get_double("FREQ_OUT")); // NOLINT
  subband_resources->set_data_host(config.get_val("DATA_HOST")); // NOLINT
  subband_resources->set_data_port(config.get_uint32("DATA_PORT")); // NOLINT
}

void RecvLmcServiceIntegrationTest::config_as_scan_configuration(ska::pst::lmc::ReceiveScanConfiguration *configuration) // NOLINT
{
    configuration->set_activation_time(config.get_val("ACTIVATION_TIME")); // NOLINT
    configuration->set_observer(config.get_val("OBSERVER")); // NOLINT
    configuration->set_projid(config.get_val("PROJID")); // NOLINT
    configuration->set_pnt_id(config.get_val("PNT_ID")); // NOLINT
    configuration->set_subarray_id(config.get_val("SUBARRAY_ID")); // NOLINT
    configuration->set_source(config.get_val("SOURCE")); // NOLINT
    configuration->set_itrf(config.get_val("ITRF")); // NOLINT
    if (config.has("BMAJ"))
    {
        configuration->set_bmaj(config.get_float("BMAJ")); // NOLINT
    }
    if (config.has("BMIN"))
    {
        configuration->set_bmin(config.get_float("BMIN")); // NOLINT
    }
    configuration->set_coord_md(config.get_val("COORD_MD")); // NOLINT
    configuration->set_equinox(config.get_val("EQUINOX")); // NOLINT
    configuration->set_stt_crd1(config.get_val("STT_CRD1")); // NOLINT
    configuration->set_stt_crd2(config.get_val("STT_CRD2")); // NOLINT
    configuration->set_trk_mode(config.get_val("TRK_MODE")); // NOLINT
    configuration->set_scanlen_max(config.get_int32("SCANLEN_MAX")); // NOLINT
    if (config.has("TEST_VECTOR"))
    {
        configuration->set_test_vector(config.get_val("TEST_VECTOR")); // NOLINT
    }
    configuration->set_execution_block_id(config.get_val("EB_ID")); // NOLINT
}

auto RecvLmcServiceIntegrationTest::configure_beam(bool dry_run) -> grpc::Status
{
    ska::pst::lmc::ConfigureBeamRequest request; // NOLINT
    request.set_dry_run(dry_run);
    auto beam_configuration = request.mutable_beam_configuration(); // NOLINT
    auto resources_msg = beam_configuration->mutable_receive(); // NOLINT
    config_as_resources(resources_msg); // NOLINT

    return configure_beam(request); // NOLINT
}

auto RecvLmcServiceIntegrationTest::configure_beam(const ska::pst::lmc::ConfigureBeamRequest &request) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ConfigureBeamResponse response; // NOLINT

    return _stub->configure_beam(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::get_beam_configuration(
    ska::pst::lmc::GetBeamConfigurationResponse* response
) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GetBeamConfigurationRequest request; // NOLINT

    return _stub->get_beam_configuration(&context, request, response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::deconfigure_beam() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::DeconfigureBeamRequest request; // NOLINT
    ska::pst::lmc::DeconfigureBeamResponse response; // NOLINT

    return _stub->deconfigure_beam(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::configure_scan(bool dry_run) -> grpc::Status
{
    ska::pst::lmc::ConfigureScanRequest request; // NOLINT
    request.set_dry_run(dry_run);
    auto scan_configuration = request.mutable_scan_configuration(); // NOLINT
    auto resources_msg = scan_configuration->mutable_receive(); // NOLINT
    config_as_scan_configuration(resources_msg); // NOLINT

    return configure_scan(request); // NOLINT
}

auto RecvLmcServiceIntegrationTest::configure_scan(const ska::pst::lmc::ConfigureScanRequest &request) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ConfigureScanResponse response; // NOLINT

    return _stub->configure_scan(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::get_scan_configuration(
    ska::pst::lmc::GetScanConfigurationResponse* response
) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GetScanConfigurationRequest request; // NOLINT

    return _stub->get_scan_configuration(&context, request, response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::deconfigure_scan() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::DeconfigureScanRequest request; // NOLINT
    ska::pst::lmc::DeconfigureScanResponse response; // NOLINT

    return _stub->deconfigure_scan(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::start_scan(uint64_t scan_id) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::StartScanRequest request; // NOLINT
    request.set_scan_id(scan_id); // NOLINT
    ska::pst::lmc::StartScanResponse response; // NOLINT

    return _stub->start_scan(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::stop_scan() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::StopScanRequest request; // NOLINT
    ska::pst::lmc::StopScanResponse response; // NOLINT

    return _stub->stop_scan(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::abort() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::AbortRequest request; // NOLINT
    ska::pst::lmc::AbortResponse response; // NOLINT

    return _stub->abort(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::go_to_fault() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GoToFaultRequest request; // NOLINT
    ska::pst::lmc::GoToFaultResponse response; // NOLINT

    return _stub->go_to_fault(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::reset() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::ResetRequest request; // NOLINT
    ska::pst::lmc::ResetResponse response; // NOLINT

    return _stub->reset(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::restart() -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::RestartRequest request; // NOLINT
    ska::pst::lmc::RestartResponse response; // NOLINT

    return _stub->restart(&context, request, &response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::get_state(ska::pst::lmc::GetStateResponse* response) -> grpc::Status
{
    grpc::ClientContext context; // NOLINT
    ska::pst::lmc::GetStateRequest request; // NOLINT

    return _stub->get_state(&context, request, response); // NOLINT
}

auto RecvLmcServiceIntegrationTest::set_log_level(
    ska::pst::lmc::LogLevel required_log_level
) -> grpc::Status
{
    grpc::ClientContext context;
    ska::pst::lmc::SetLogLevelResponse response;
    ska::pst::lmc::SetLogLevelRequest request;
    request.set_log_level(required_log_level);
    return _stub->set_log_level(&context, request, &response);
}

auto RecvLmcServiceIntegrationTest::get_log_level(
    ska::pst::lmc::GetLogLevelResponse& response
) -> grpc::Status
{
    grpc::ClientContext context;
    ska::pst::lmc::GetLogLevelRequest request;
    return _stub->get_log_level(&context, request, &response);
}

void RecvLmcServiceIntegrationTest::assert_state(
    ska::pst::lmc::ObsState expected_state
) // NOLINT
{
    ska::pst::lmc::GetStateResponse get_state_response; // NOLINT
    auto status = get_state(&get_state_response); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_EQ(get_state_response.state(), expected_state); // NOLINT

    switch (expected_state) {
        case ska::pst::lmc::ObsState::EMPTY:
            assert_manager_state(ska::pst::common::Idle);
            break;
        case ska::pst::lmc::ObsState::IDLE:
            assert_manager_state(ska::pst::common::BeamConfigured);
            break;
        case ska::pst::lmc::ObsState::READY:
            assert_manager_state(ska::pst::common::ScanConfigured);
            break;
        case ska::pst::lmc::ObsState::SCANNING:
            assert_manager_state(ska::pst::common::Scanning);
            break;
        case ska::pst::lmc::ObsState::FAULT:
            assert_manager_state(ska::pst::common::RuntimeError);
            break;
        default:
            // we can't assert the manager state
            break;
    }
}


void RecvLmcServiceIntegrationTest::assert_log_level(
    ska::pst::lmc::LogLevel expected_loglevel
)
{
    switch (expected_loglevel) {
        case ska::pst::lmc::LogLevel::INFO:
            ASSERT_EQ(spdlog::level::info, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::DEBUG:
            ASSERT_EQ(spdlog::level::debug, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::WARNING:
            ASSERT_EQ(spdlog::level::warn, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::CRITICAL:
            ASSERT_EQ(spdlog::level::critical, spdlog::get_level());
            break;
        case ska::pst::lmc::LogLevel::ERROR:
            ASSERT_EQ(spdlog::level::err, spdlog::get_level());
            break;
        default:
            FAIL() << "LogLevel enum not implemented yet";
            break;
    }
}

void RecvLmcServiceIntegrationTest::assert_manager_state(
    ska::pst::common::State expected_state
)
{
    auto curr_state = _recv->get_state();
    EXPECT_EQ(curr_state, expected_state); // NOLINT
}

void LmcServiceTestReceiver::configure_beam(
    const ska::pst::common::AsciiHeader &config
) // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Configure beam error."); // NOLINT
    }
    UDPReceiveDB::configure_beam(config); // NOLINT
}

void LmcServiceTestReceiver::deconfigure_beam() // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Deconfigure beam error."); // NOLINT
    }
    UDPReceiveDB::deconfigure_beam(); // NOLINT
}

auto LmcServiceTestReceiver::get_beam_configuration() -> ska::pst::common::AsciiHeader&
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Getting beam configuration error."); // NOLINT
    }
    return UDPReceiveDB::get_beam_configuration(); // NOLINT
}

void LmcServiceTestReceiver::configure_scan(
    const ska::pst::common::AsciiHeader &config
) // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Configure scan error."); // NOLINT
    }
    UDPReceiveDB::configure_scan(config); // NOLINT
}

void LmcServiceTestReceiver::deconfigure_scan() // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Deconfigure scan error."); // NOLINT
    }
    UDPReceiveDB::deconfigure_scan(); // NOLINT
}

auto LmcServiceTestReceiver::get_scan_configuration() -> ska::pst::common::AsciiHeader&
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Get scan configuration error."); // NOLINT
    }
    return UDPReceiveDB::get_scan_configuration(); // NOLINT
}

void LmcServiceTestReceiver::start_scan(const ska::pst::common::AsciiHeader &config) // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Start scan error."); // NOLINT
    }
    UDPReceiveDB::start_scan(config); // NOLINT
}

void LmcServiceTestReceiver::stop_scan() // NOLINT
{
    if (_throw_on_next_call)
    {
        throw std::runtime_error("Stop scan error."); // NOLINT
    }
    UDPReceiveDB::stop_scan(); // NOLINT
}

void LmcServiceTestReceiver::throw_exception_on_next_call() // NOLINT
{
    _throw_on_next_call = true; // NOLINT
}

void LmcServiceTestReceiver::reset_throw_exception() // NOLINT
{
    _throw_on_next_call = false; // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, start_scan_stop_scan) // NOLINT
{
    _service->start(); // NOLINT
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, _recv->get_state()); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - configuring beam"); // NOLINT
    auto status = configure_beam(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - beam configured"); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - configuring scan"); // NOLINT
    status = configure_scan(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::READY); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - scan configured"); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - starting scan"); // NOLINT
    EXPECT_FALSE(_recv->is_scanning()); // NOLINT
    static constexpr uint64_t scan_id = 12345; // NOLINT
    EXPECT_EQ(_recv->get_scan_id(), UINT64_MAX); // NOLINT
    status = start_scan(scan_id); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_EQ(_recv->get_scan_id(), scan_id); // NOLINT
    assert_state(ska::pst::lmc::ObsState::SCANNING); // NOLINT
    EXPECT_TRUE(_recv->is_scanning()); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - scanning"); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - stopping scan"); // NOLINT
    status = stop_scan(); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::READY); // NOLINT
    EXPECT_EQ(_recv->get_scan_id(), UINT64_MAX); // NOLINT
    EXPECT_FALSE(_recv->is_scanning()); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::start_scan_stop_scan - stopped scanning"); // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, go_to_fault_when_runtime_error_encountered_deconfigure_beam) // NOLINT
{
    // Induce error in config to simulate RuntimeError
    config.set_val("BEAM_ID","!@#$%");

    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    auto status = configure_beam(); // NOLINT
    EXPECT_FALSE(status.ok()); // NOLINT
    EXPECT_FALSE(_recv->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, reset_when_in_fault_state) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    auto status = go_to_fault();
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::FAULT); // NOLINT

    status = reset();
    assert_state(ska::pst::lmc::ObsState::EMPTY);
}

TEST_F(RecvLmcServiceIntegrationTest, validate_configure_beam) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_beam - validating beam config"); // NOLINT
    auto status = configure_beam(DRY_RUN); // NOLINT

    EXPECT_TRUE(status.ok()); // NOLINT
    EXPECT_FALSE(_handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_recv->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_beam - beam config validated"); // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, validate_configure_beam_invalid_config) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_beam_invalid_config - validating beam config"); // NOLINT
    config.set("DATA_KEY", "@!00");
    auto status = configure_beam(DRY_RUN); // NOLINT

    EXPECT_FALSE(status.ok()); // NOLINT

    EXPECT_EQ(grpc::StatusCode::FAILED_PRECONDITION, status.error_code()); // NOLINT
    EXPECT_EQ("Error in validating beam configuration: DATA_KEY with value @!00 failed validation: failed regex validation of \"^[a-f0-9]{4,}$\"", status.error_message()); // NOLINT

    ska::pst::lmc::Status lmc_status;
    lmc_status.ParseFromString(status.error_details());
    EXPECT_EQ(ska::pst::lmc::ErrorCode::INVALID_REQUEST, lmc_status.code()); // NOLINT
    EXPECT_EQ(status.error_message(), lmc_status.message()); // NOLINT

    EXPECT_FALSE(_handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_recv->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_beam_invalid_config - beam config validated"); // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, validate_configure_beam_invalid_udp_format_data) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_beam_invalid_udp_format_data - validating beam config"); // NOLINT
    config.set("NBIT", 42); // NOLINT
    auto status = configure_beam(DRY_RUN); // NOLINT

    EXPECT_FALSE(status.ok()); // NOLINT

    EXPECT_EQ(grpc::StatusCode::FAILED_PRECONDITION, status.error_code()); // NOLINT
    EXPECT_EQ("Error in validating beam configuration: NBIT with value 42 failed validation: not equal to 16", status.error_message()); // NOLINT

    ska::pst::lmc::Status lmc_status;
    lmc_status.ParseFromString(status.error_details());
    EXPECT_EQ(ska::pst::lmc::ErrorCode::INVALID_REQUEST, lmc_status.code()); // NOLINT
    EXPECT_EQ(status.error_message(), lmc_status.message()); // NOLINT

    EXPECT_FALSE(_handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_recv->is_beam_configured()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_beam_invalid_udp_format_data - beam config validated"); // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, validate_configure_scan) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::EMPTY); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_scan - configuring beam"); // NOLINT
    auto status = configure_beam(); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_scan - beam configured"); // NOLINT

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_scan - validating scan configuration"); // NOLINT
    status = configure_scan(DRY_RUN); // NOLINT
    EXPECT_TRUE(status.ok()); // NOLINT
    assert_state(ska::pst::lmc::ObsState::IDLE); // NOLINT
    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::validate_configure_scan - scan config validated"); // NOLINT
}

TEST_F(RecvLmcServiceIntegrationTest, set_and_get_log_levels) // NOLINT
{
    EXPECT_TRUE(_service->is_running()); // NOLINT
    ska::pst::lmc::GetLogLevelResponse response;

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::set_loglevel - DEBUG"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::DEBUG);
    assert_log_level(ska::pst::lmc::LogLevel::DEBUG);
    auto status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::DEBUG, response.log_level());

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::set_log_level - INFO"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::INFO);
    assert_log_level(ska::pst::lmc::LogLevel::INFO);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::INFO, response.log_level());

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::set_log_level - WARNING"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::WARNING);
    assert_log_level(ska::pst::lmc::LogLevel::WARNING);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::WARNING, response.log_level());

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::set_log_level - CRITICAL"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::CRITICAL);
    assert_log_level(ska::pst::lmc::LogLevel::CRITICAL);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::CRITICAL, response.log_level());

    SPDLOG_TRACE("RecvLmcServiceIntegrationTest::set_log_level - ERROR"); // NOLINT
    set_log_level(ska::pst::lmc::LogLevel::ERROR);
    assert_log_level(ska::pst::lmc::LogLevel::ERROR);
    status = get_log_level(response);
    EXPECT_TRUE(status.ok());
    ASSERT_EQ(ska::pst::lmc::LogLevel::ERROR, response.log_level());
}

} // namespace ska::pst::recv::test
