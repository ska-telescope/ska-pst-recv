/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <memory>
#include <grpc++/grpc++.h>
#include <gtest/gtest.h>
#include "ska/pst/smrb/DataBlockCreate.h"
#include "ska/pst/common/lmc/LmcService.h"
#include "ska/pst/recv/formats/UDPFormat.h"
#include "ska/pst/recv/lmc/RecvLmcServiceHandler.h"
#include "ska/pst/recv/clients/UDPReceiveDB.h"
#include "ska/pst/recv/network/UDPSocketReceive.h"

#ifndef SKA_PST_RECV_TESTS_LmcServiceTest_h
#define SKA_PST_RECV_TESTS_LmcServiceTest_h

namespace ska::pst::recv::test {

/**
 * @brief Test Receiver to use for tests
 *
 * @details
 *
 */
class LmcServiceTestReceiver : public ska::pst::recv::UDPReceiveDB
{
    public:
      LmcServiceTestReceiver(std::shared_ptr<SocketReceive> sock, const std::string& _data_host) : ska::pst::recv::UDPReceiveDB(sock, _data_host) {}
      virtual ~LmcServiceTestReceiver() = default;

      void configure_beam(const ska::pst::common::AsciiHeader& config);
      void configure_scan(const ska::pst::common::AsciiHeader& config);
      void start_scan(const ska::pst::common::AsciiHeader& config);
      void stop_scan();
      void deconfigure_beam();
      void deconfigure_scan();

      void perform_scan() {
        while (is_scanning() && socket->still_receiving())
        {
          usleep(100000);
        }
      }

      ska::pst::common::AsciiHeader& get_beam_configuration() override;
      ska::pst::common::AsciiHeader& get_scan_configuration() override;

      void throw_exception_on_next_call();
      void reset_throw_exception();

      void set_stats(std::shared_ptr<UDPStats> stats) {
        _overriden_stats = stats;
        _overriden_stats->configure(format.get());
      }

      const UDPStats& get_stats() override {
        if (_overriden_stats != nullptr)
        {
            return *_overriden_stats;
        }
        return Receiver::get_stats();
      }

    protected:
      bool _throw_on_next_call{false};
      std::shared_ptr<UDPStats> _overriden_stats{nullptr};

};

/**
 * @brief Test the DataBlock class
 *
 * @details
 *
 */
class RecvLmcServiceIntegrationTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

        grpc::Status configure_beam(bool dry_run = false);
        grpc::Status configure_beam(const ska::pst::lmc::ConfigureBeamRequest &request);
        grpc::Status get_beam_configuration(ska::pst::lmc::GetBeamConfigurationResponse* response);
        grpc::Status deconfigure_beam();

        grpc::Status configure_scan(bool dry_run = false);
        grpc::Status configure_scan(const ska::pst::lmc::ConfigureScanRequest &request);
        grpc::Status deconfigure_scan();
        grpc::Status get_scan_configuration(ska::pst::lmc::GetScanConfigurationResponse* response);

        grpc::Status start_scan(uint64_t scan_id);
        grpc::Status stop_scan();

        grpc::Status abort();
        grpc::Status reset();
        grpc::Status restart();
        grpc::Status go_to_fault();

        grpc::Status get_state();
        
        // set log level
        grpc::Status set_log_level(ska::pst::lmc::LogLevel required_log_level);
        grpc::Status get_log_level(ska::pst::lmc::GetLogLevelResponse& response);

        void config_as_resources(ska::pst::lmc::ReceiveBeamConfiguration *resources);
        void config_as_scan_configuration(ska::pst::lmc::ReceiveScanConfiguration *configuration);

        grpc::Status get_state(ska::pst::lmc::GetStateResponse*);
        void assert_state(ska::pst::lmc::ObsState state);
        void assert_manager_state(ska::pst::common::State);
        void assert_log_level(ska::pst::lmc::LogLevel);

    public:
        RecvLmcServiceIntegrationTest();
        ~RecvLmcServiceIntegrationTest() = default;

        int _port = 0;
        std::shared_ptr<ska::pst::common::LmcService> _service{nullptr};
        std::shared_ptr<ska::pst::recv::test::LmcServiceTestReceiver> _recv{nullptr};
        std::shared_ptr<ska::pst::recv::RecvLmcServiceHandler> _handler{nullptr};
        std::shared_ptr<grpc::Channel> _channel{nullptr};
        std::shared_ptr<ska::pst::lmc::PstLmcService::Stub> _stub{nullptr};

        std::shared_ptr<UDPFormat> format{nullptr};
        ska::pst::common::AsciiHeader config;

        std::string db_key;
        std::string wb_key;

        ska::pst::smrb::DataBlockCreate db;
        ska::pst::smrb::DataBlockCreate wb;

        std::shared_ptr<SocketReceive> sock{nullptr};

        uint64_t header_nbufs{4};

        uint64_t header_bufsz{4096};

        uint64_t data_nbufs{4};

        uint64_t weights_nbufs{4};

};

} // namespace ska::pst::recv::test

#endif // SKA_PST_RECV_TESTS_LmcServiceTest_h
